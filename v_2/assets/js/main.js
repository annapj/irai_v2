$(document).ready(function() {
  $("#owl-demo").owlCarousel({
  	singleItem:true,
    navigation: true,
    navigationText: [
      "<i class='icon-chevron-left icon-white'></i>",
      "<i class='icon-chevron-right icon-white'></i>"
      ],
  });
 
});

function popupCenter(name,center){
    var w_win = $(window).width();
    var h_win = $(window).height();
    var w_name= (w_win - $('#'+name).width())/2;

    $('#bg-popup').show();
    if(center==true){
        var h_name= (h_win - $('#'+name).height())/2;
        $('#'+name).css({
            'left' : w_name,
            'top' : h_name,
            'display' : 'block',
        });
    }else{
        $('#'+name).css({
            'left' : w_name,
            'top' : '269px', 
            'display' : 'block',
        });
    }
}

function closePopup(){
    $('#bg-popup').hide();
    $('.popup').hide();
}
