$(document).ready(function() {
  $("#owl-demo").owlCarousel({
  	singleItem:true,
    navigation: true,
    navigationText: [
      "<i class='icon-chevron-left icon-white'></i>",
      "<i class='icon-chevron-right icon-white'></i>"
      ],
  });
 
});
