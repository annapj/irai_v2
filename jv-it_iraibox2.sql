/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : jv-it_iraibox2

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-11-03 12:26:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for active_token
-- ----------------------------
DROP TABLE IF EXISTS `active_token`;
CREATE TABLE `active_token` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of active_token
-- ----------------------------
INSERT INTO `active_token` VALUES ('3', 'kimcuu1992@gmail.com', 'ryWQK0JI3R', '2015-08-27 14:38:28', '1');
INSERT INTO `active_token` VALUES ('6', 'kimcuu1992@gmail.com', 'Oprd1XfakU', '2015-08-27 15:04:49', '1');
INSERT INTO `active_token` VALUES ('10', 'kimcuu1992@gmail.com', 'kfynSLYH5p', '2015-08-27 15:22:48', '1');
INSERT INTO `active_token` VALUES ('11', 'kimcuu1992@gmail.com', 'dfJKEBicWt', '2015-08-27 16:15:51', '1');
INSERT INTO `active_token` VALUES ('12', 'kimcuu1992@gmail.com', 'L7gi6VRsTB', '2015-08-27 16:16:54', '1');
INSERT INTO `active_token` VALUES ('13', 'kimcuu1992@gmail.com', 'HCDPqnIyS2', '2015-08-27 16:20:10', '1');
INSERT INTO `active_token` VALUES ('14', 'kimcuu1992@gmail.com', 'bBUFWeY4Dv', '2015-08-28 14:26:06', '1');
INSERT INTO `active_token` VALUES ('15', 'kimcuu1992@gmail.com', 'UDLsSIn5Om', '2015-08-28 14:27:14', '1');
INSERT INTO `active_token` VALUES ('16', 'kimcuu1992@gmail.com', 'AX2Uxmcrpo', '2015-08-28 14:30:17', '1');
INSERT INTO `active_token` VALUES ('17', 'kimcuu1992@gmail.com', 'g6RO15M7Ny', '2015-08-28 14:32:03', '1');
INSERT INTO `active_token` VALUES ('18', 'kimcuu1992@gmail.com', 'gzQlF46Bqk', '2015-08-28 14:34:37', '1');
INSERT INTO `active_token` VALUES ('19', 'kimcuu1992@gmail.com', 'RhTLS1UiWH', '2015-09-06 02:14:06', '1');
INSERT INTO `active_token` VALUES ('20', 'kimcuu1992@gmail.com', 'Hy2v6Uox7A', '2015-09-06 02:19:12', '1');
INSERT INTO `active_token` VALUES ('21', 'kimcuu1992@gmail.com', 'aZ4vzn8m1q', '2015-09-06 02:36:02', '1');
INSERT INTO `active_token` VALUES ('22', 'kimcuu1992@gmail.com', 'y2rslwPLfQ', '2015-09-06 02:36:11', '1');
INSERT INTO `active_token` VALUES ('23', 'kimcuu1992@gmail.com', 'QTmuaqi04Y', '2015-09-06 02:36:14', '1');
INSERT INTO `active_token` VALUES ('24', 'kimcuu1992@gmail.com', '4rwzcFbeMj', '2015-09-06 02:36:18', '1');
INSERT INTO `active_token` VALUES ('25', 'kimcuu1992@gmail.com', '4fTJ1YB2CE', '2015-09-06 02:36:21', '1');
INSERT INTO `active_token` VALUES ('26', 'kimcuu1992@gmail.com', 'cHIurhtTXx', '2015-09-06 02:36:25', '1');
INSERT INTO `active_token` VALUES ('27', 'kimcuu1992@gmail.com', 'jVdvDpTGEu', '2015-09-06 02:36:29', '1');
INSERT INTO `active_token` VALUES ('28', 'kimcuu1992@gmail.com', 'sknJcOwKXA', '2015-09-06 02:36:33', '1');
INSERT INTO `active_token` VALUES ('29', 'kimcuu1992@gmail.com', '5GwrYD0Rtg', '2015-09-06 02:36:37', '1');
INSERT INTO `active_token` VALUES ('30', 'kimcuu1992@gmail.com', 'prCmGYkcNh', '2015-09-06 02:36:40', '1');
INSERT INTO `active_token` VALUES ('31', 'kimcuu1992@gmail.com', 'TZUCmtYxu1', '2015-09-06 02:36:44', '1');
INSERT INTO `active_token` VALUES ('32', 'kimcuu1992@gmail.com', 'RXNIV4Hwhi', '2015-09-06 02:36:48', '1');
INSERT INTO `active_token` VALUES ('33', 'kimcuu1992@gmail.com', 'r4qY8wLpjJ', '2015-09-06 02:36:51', '1');
INSERT INTO `active_token` VALUES ('34', 'kimcuu1992@gmail.com', 'QxRLXoIcDG', '2015-09-06 02:36:55', '1');
INSERT INTO `active_token` VALUES ('35', 'kimcuu1992@gmail.com', 'pGYvkxhLdT', '2015-09-06 02:36:58', '1');
INSERT INTO `active_token` VALUES ('36', 'kimcuu1992@gmail.com', 'ieHfZ0JUNg', '2015-09-06 02:38:05', '1');
INSERT INTO `active_token` VALUES ('37', 'kimcuu1992@gmail.com', 'dQWCgFbErp', '2015-09-06 02:38:09', '1');
INSERT INTO `active_token` VALUES ('38', 'kimcuu1992@gmail.com', 'gqa5fCnmpx', '2015-09-06 02:38:12', '1');
INSERT INTO `active_token` VALUES ('39', 'kimcuu1992@gmail.com', 'CnG4Bu5MlO', '2015-09-06 02:38:16', '1');
INSERT INTO `active_token` VALUES ('40', 'kimcuu1992@gmail.com', 'ZfRLXEjl63', '2015-09-06 02:38:19', '1');
INSERT INTO `active_token` VALUES ('41', 'kimcuu1992@gmail.com', 'RTX0lLoYmE', '2015-09-06 02:38:23', '1');
INSERT INTO `active_token` VALUES ('42', 'kimcuu1992@gmail.com', 'n4L8tz0SC5', '2015-09-06 02:38:26', '1');
INSERT INTO `active_token` VALUES ('43', 'kimcuu1992@gmail.com', 'gJSn6vy3me', '2015-09-06 02:38:29', '1');
INSERT INTO `active_token` VALUES ('44', 'kimcuu1992@gmail.com', 'GDfZFBU6qk', '2015-09-06 02:38:32', '1');
INSERT INTO `active_token` VALUES ('45', 'kimcuu1992@gmail.com', 'IMTUkqjzS1', '2015-09-06 02:38:36', '1');
INSERT INTO `active_token` VALUES ('46', 'kimcuu1992@gmail.com', 'VtCk8XGBK4', '2015-09-06 08:16:35', '1');
INSERT INTO `active_token` VALUES ('47', 'kimcuu1992@gmail.com', '1EG5V9z4vw', '2015-09-06 08:23:29', '1');
INSERT INTO `active_token` VALUES ('48', 'kimcuu1992@gmail.com', 'u9RQ3ApS2N', '2015-09-07 09:52:30', '1');
INSERT INTO `active_token` VALUES ('49', 'kimcuu1992@gmail.com', 'N0oxns5h6M', '2015-09-11 21:10:02', '1');
INSERT INTO `active_token` VALUES ('50', 'kimcuu1992@gmail.com', 'jn8M9am6R4', '2015-09-11 21:25:40', '1');
INSERT INTO `active_token` VALUES ('51', 'kimcuu1992@gmail.com', 'Rons4NimGk', '2015-09-11 21:26:42', '1');
INSERT INTO `active_token` VALUES ('52', 'kimcuu1992@gmail.com', 'vIZOtre5kf', '2015-09-12 13:38:44', '1');
INSERT INTO `active_token` VALUES ('53', 'kimcuu1992@gmail.com', 'lFrG7ME4vk', '2015-09-12 13:40:38', '1');
INSERT INTO `active_token` VALUES ('54', 'kimcuu1992@gmail.com', 'X4R69Z5dP3', '2015-09-12 13:42:42', '1');
INSERT INTO `active_token` VALUES ('55', 'kimcuu1992@gmail.com', 'SOHFPDUm65', '2015-09-14 14:23:01', '1');
INSERT INTO `active_token` VALUES ('56', 'kimcuu1992@gmail.com', 'w9aE4pAlVq', '2015-09-14 15:11:48', '1');
INSERT INTO `active_token` VALUES ('57', 'kimcuu1992@gmail.com', 'bvFtMrjQKJ', '2015-09-16 13:12:09', '1');
INSERT INTO `active_token` VALUES ('58', 'kimcuu1992@gmail.com', 'YTArnJfVM1', '2015-09-16 13:12:13', '1');
INSERT INTO `active_token` VALUES ('59', 'kimcuu1992@gmail.com', 'RBaqujL3Xy', '2015-09-16 13:29:48', '1');
INSERT INTO `active_token` VALUES ('60', 'kimcuu1992@gmail.com', 'a4M5Qswr1N', '2015-09-16 13:30:34', '1');
INSERT INTO `active_token` VALUES ('61', 'kimcuu1992@gmail.com', 'XrdLPKjtWu', '2015-09-16 13:31:17', '1');
INSERT INTO `active_token` VALUES ('62', 'kimcuu1992@gmail.com', 'QapRPdExro', '2015-09-16 13:32:11', '1');
INSERT INTO `active_token` VALUES ('63', 'kimcuu1992@gmail.com', 'PFD8adnJeX', '2015-09-16 13:34:26', '1');
INSERT INTO `active_token` VALUES ('64', 'kimcuu1992@gmail.com', 'tDHI3O2jLM', '2015-09-16 13:35:48', '1');
INSERT INTO `active_token` VALUES ('65', 'kimcuu1992@gmail.com', 'dJghGu4f5L', '2015-09-16 16:41:54', '1');
INSERT INTO `active_token` VALUES ('66', 'kimcuu1992@gmail.com', 'JjPsChGKwi', '2015-09-16 16:42:13', '1');
INSERT INTO `active_token` VALUES ('67', 'kimcuu1992@gmail.com', 'YOzfD3CKmP', '2015-09-16 16:42:32', '1');
INSERT INTO `active_token` VALUES ('68', 'kimcuu1992@gmail.com', 'whFvz9AHTe', '2015-09-16 16:42:57', '1');
INSERT INTO `active_token` VALUES ('69', 'kimcuu1992@gmail.com', 'LwNoxPSkgq', '2015-09-17 08:12:53', '1');
INSERT INTO `active_token` VALUES ('70', 'kimcuu1992@gmail.com', 'E2uAqDjsHh', '2015-09-17 09:17:39', '1');
INSERT INTO `active_token` VALUES ('71', 'kimcuu1992@gmail.com', '2Pz3UT71xS', '2015-09-17 15:08:54', '1');
INSERT INTO `active_token` VALUES ('72', 'kimcuu1992@gmail.com', '6e7M8inxCk', '2015-09-17 15:19:02', '1');
INSERT INTO `active_token` VALUES ('73', 'kimcuu1992@gmail.com', 'A8yfc3rXsQ', '2015-09-17 15:19:45', '1');
INSERT INTO `active_token` VALUES ('74', 'kimcuu1992@gmail.com', 'TyY7uQ0iaE', '2015-09-17 15:31:38', '1');
INSERT INTO `active_token` VALUES ('75', 'kimcuu1992@gmail.com', 'fVL7AZPucF', '2015-09-17 15:32:06', '1');
INSERT INTO `active_token` VALUES ('76', 'kimcuu1992@gmail.com', 'Ohi8WoPM2w', '2015-09-17 15:33:44', '1');
INSERT INTO `active_token` VALUES ('77', 'kimcuu1992@gmail.com', '6IFPG8ZQwf', '2015-09-17 15:34:11', '1');
INSERT INTO `active_token` VALUES ('78', 'kimcuu1992@gmail.com', 'jWv6R10s78', '2015-09-17 15:42:12', '1');
INSERT INTO `active_token` VALUES ('79', 'kimcuu1992@gmail.com', '0KpwhS9AQo', '2015-09-17 15:49:34', '1');
INSERT INTO `active_token` VALUES ('80', 'kimcuu1992@gmail.com', 'yfhqMO7gze', '2015-09-17 15:50:38', '1');
INSERT INTO `active_token` VALUES ('81', 'kimcuu1992@gmail.com', 'iLsZ3ONInB', '2015-09-17 15:50:41', '1');
INSERT INTO `active_token` VALUES ('82', 'kimcuu1992@gmail.com', 'r7d5VawNky', '2015-09-17 15:57:35', '1');
INSERT INTO `active_token` VALUES ('83', 'kimcuu1992@gmail.com', 'LG2c081DnS', '2015-09-17 15:57:39', '1');
INSERT INTO `active_token` VALUES ('84', 'kimcuu1992@gmail.com', 'XiF7PtT8cq', '2015-09-17 16:03:58', '1');
INSERT INTO `active_token` VALUES ('85', 'kimcuu1992@gmail.com', 'YzIp0KwXyk', '2015-09-17 16:04:44', '1');
INSERT INTO `active_token` VALUES ('86', 'kimcuu1992@gmail.com', 'zTY0MhZXCU', '2015-09-17 16:04:53', '1');
INSERT INTO `active_token` VALUES ('87', 'kimcuu1992@gmail.com', 'pz6bK52R7I', '2015-09-17 16:05:30', '1');
INSERT INTO `active_token` VALUES ('88', 'kimcuu1992@gmail.com', 'imUdVu0Xsk', '2015-09-18 22:54:43', '1');
INSERT INTO `active_token` VALUES ('89', 'kimcuu1992@gmail.com', 'TGOCyMn79K', '2015-09-19 19:24:34', '1');
INSERT INTO `active_token` VALUES ('95', 'kimcuu1992@gmail.com', 'FHnXBLpexg', '2015-09-19 21:59:32', '1');
INSERT INTO `active_token` VALUES ('96', 'kimcuu1992@gmail.com', '31VQfIb4Gv', '2015-09-19 21:59:37', '1');
INSERT INTO `active_token` VALUES ('97', 'kimcuu1992@gmail.com', 'XdQgz71WCR', '2015-09-19 22:02:59', '1');
INSERT INTO `active_token` VALUES ('98', 'kimcuu1992@gmail.com', 'IlhYQDy7Mp', '2015-09-19 22:03:04', '1');
INSERT INTO `active_token` VALUES ('99', 'kimcuu1992@gmail.com', 'uXlReCIorK', '2015-09-19 22:03:49', '1');
INSERT INTO `active_token` VALUES ('102', 'kimcuu1992@gmail.com', 'Xk7pTAWNLK', '2015-09-19 22:14:46', '1');
INSERT INTO `active_token` VALUES ('104', 'kimcuu1992@gmail.com', 'MmHqKIdAzy', '2015-09-22 10:58:01', '1');
INSERT INTO `active_token` VALUES ('105', 'kimcuu1992@gmail.com', 'ihxaJFC8dD', '2015-09-22 10:58:27', '1');
INSERT INTO `active_token` VALUES ('106', 'kimcuu1992@gmail.com', 'dJ0fNmi47S', '2015-09-22 11:01:07', '1');
INSERT INTO `active_token` VALUES ('107', 'kimcuu1992@gmail.com', '5krOsSlf0o', '2015-09-22 11:01:12', '1');
INSERT INTO `active_token` VALUES ('108', 'kimcuu1992@gmail.com', 'LXhiUK64jY', '2015-09-22 11:01:16', '1');
INSERT INTO `active_token` VALUES ('110', 'kimcuu1992@gmail.com', 'NiYbmZzpVF', '2015-09-22 11:23:15', '1');
INSERT INTO `active_token` VALUES ('111', 'kimcuu1992@gmail.com', 'EZtR68rD2K', '2015-09-22 11:30:29', '1');
INSERT INTO `active_token` VALUES ('112', 'kimcuu1992@gmail.com', 'uXlReCIorK', '2015-09-22 14:46:14', '1');
INSERT INTO `active_token` VALUES ('117', 'kimcuu1992@gmail.com', 'Qdw8fYRDLxFK0aszWo9y', '2015-09-22 15:33:52', '1');
INSERT INTO `active_token` VALUES ('118', 'kimcuu1992@gmail.com', '1VXbD7LUASGh3fMTe62q', '2015-09-22 15:33:59', '1');
INSERT INTO `active_token` VALUES ('119', 'kimcuu1992@gmail.com', 'PWb71SwrUHQ58KpVisR9', '2015-09-22 15:34:26', '1');
INSERT INTO `active_token` VALUES ('123', 'kimcuu1992@gmail.com', 'Rp75gMDb8xJuLdXnC1hN', '2015-09-22 17:06:02', '1');
INSERT INTO `active_token` VALUES ('124', 'kimcuu1992@gmail.com', 'TgnL3Go12wyVq7di0jFh', '2015-09-29 16:57:47', '0');
INSERT INTO `active_token` VALUES ('125', 'kimcuu1992@gmail.com', 'FsKBXmheAQ0kvgtzYqTn', '2015-10-09 15:09:00', '1');

-- ----------------------------
-- Table structure for admin_nqt_groups
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_groups`;
CREATE TABLE `admin_nqt_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_groups
-- ----------------------------
INSERT INTO `admin_nqt_groups` VALUES ('1', 'Root', '0|rwd,116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '1', '2012-08-28 14:51:26');

-- ----------------------------
-- Table structure for admin_nqt_logs
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_logs`;
CREATE TABLE `admin_nqt_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `function` varchar(50) DEFAULT NULL,
  `function_id` int(11) DEFAULT NULL,
  `field` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `old_value` text,
  `new_value` text,
  `account` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1162 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_logs
-- ----------------------------
INSERT INTO `admin_nqt_logs` VALUES ('1', 'banners', '62', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-07-30 10:53:59');
INSERT INTO `admin_nqt_logs` VALUES ('2', 'banners', '63', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-07-30 10:55:15');
INSERT INTO `admin_nqt_logs` VALUES ('3', 'banners', '64', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-07-30 10:56:07');
INSERT INTO `admin_nqt_logs` VALUES ('4', 'banners', '63', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-07-30 11:01:13');
INSERT INTO `admin_nqt_logs` VALUES ('5', 'banners', '62', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-07-30 11:01:13');
INSERT INTO `admin_nqt_logs` VALUES ('6', 'banners', '64', 'content', 'Update', null, 'test', 'admin', '::1', '2015-07-30 11:29:10');
INSERT INTO `admin_nqt_logs` VALUES ('7', 'banners', '65', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-07-31 08:53:32');
INSERT INTO `admin_nqt_logs` VALUES ('8', 'banners', '64', 'order', 'Update', '0', '1', 'admin', '::1', '2015-07-31 09:06:49');
INSERT INTO `admin_nqt_logs` VALUES ('9', 'banners', '65', 'order', 'Update', '0', '2', 'admin', '::1', '2015-07-31 09:07:17');
INSERT INTO `admin_nqt_logs` VALUES ('10', 'banners', '65', 'status', 'update', '1', '0', 'admin', '::1', '2015-07-31 09:10:26');
INSERT INTO `admin_nqt_logs` VALUES ('11', 'banners', '65', 'status', 'update', '0', '1', 'admin', '::1', '2015-07-31 09:10:26');
INSERT INTO `admin_nqt_logs` VALUES ('12', 'banners', '64', 'status', 'update', '1', '0', 'admin', '::1', '2015-07-31 09:10:28');
INSERT INTO `admin_nqt_logs` VALUES ('13', 'banners', '64', 'status', 'update', '0', '1', 'admin', '::1', '2015-07-31 09:10:28');
INSERT INTO `admin_nqt_logs` VALUES ('14', 'companies', '1', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-07-31 11:25:08');
INSERT INTO `admin_nqt_logs` VALUES ('15', 'companies', '1', 'email', 'Update', '', 'luan@yahoo.com', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('16', 'companies', '1', 'full_name', 'Update', '', 'Luan Nguyen', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('17', 'companies', '1', 'katakana', 'Update', '', 'alsdjlaskjd', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('18', 'companies', '1', 'company_name', 'Update', '', 'JV-IT', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('19', 'companies', '1', 'service_name', 'Update', '', 'J-SaiGon', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('20', 'companies', '1', 'logo', 'Update', '', '2015/07/994cdbbc0600ac39105f84e03d26c08e.jpg', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('21', 'companies', '1', 'description', 'Update', '', 'test', 'admin', '::1', '2015-07-31 11:25:40');
INSERT INTO `admin_nqt_logs` VALUES ('22', 'companies', '1', 'logo', 'Update', '2015/07/994cdbbc0600ac39105f84e03d26c08e.jpg', '2015/07/2b98bcb248b53e04534a8d2d1bf82c25.jpg', 'admin', '::1', '2015-07-31 11:26:01');
INSERT INTO `admin_nqt_logs` VALUES ('23', 'companies', '1', 'logo', 'Update', '2015/07/2b98bcb248b53e04534a8d2d1bf82c25.jpg', '2015/07/10f6d926410c5a2d44dba789deb3f03d.jpg', 'admin', '::1', '2015-07-31 11:27:02');
INSERT INTO `admin_nqt_logs` VALUES ('24', 'companies', '1', 'order', 'Update', '0', '1', 'admin', '::1', '2015-07-31 11:30:55');
INSERT INTO `admin_nqt_logs` VALUES ('25', 'companies', '1', 'katakana', 'Update', 'alsdjlaskjd', 'ミタニ', 'admin', '::1', '2015-08-03 09:32:16');
INSERT INTO `admin_nqt_logs` VALUES ('26', 'companies', '1', 'full_name', 'Update', 'Luan Nguyen', 'ミタニ', 'admin', '::1', '2015-08-03 09:38:47');
INSERT INTO `admin_nqt_logs` VALUES ('27', 'companies', '1', 'full_name', 'Update', 'ミタニ', '三谷', 'admin', '::1', '2015-08-03 09:39:03');
INSERT INTO `admin_nqt_logs` VALUES ('28', 'members', '1', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-08-03 11:13:30');
INSERT INTO `admin_nqt_logs` VALUES ('29', 'members', '1', 'password', 'Update', 'e10adc3949ba59abbe56e057f20f883e', '202cb962ac59075b964b07152d234b70', 'admin', '::1', '2015-08-04 10:38:45');
INSERT INTO `admin_nqt_logs` VALUES ('30', 'job_recruiments', '1', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-08-04 16:57:51');
INSERT INTO `admin_nqt_logs` VALUES ('31', 'job_recruiments', '1', 'content', 'Update', 'test', 'test asd', 'admin', '::1', '2015-08-05 08:41:03');
INSERT INTO `admin_nqt_logs` VALUES ('32', 'companies', '1', 'logo', 'Update', '2015/07/10f6d926410c5a2d44dba789deb3f03d.jpg', '2015/08/b42aa98d5dc41e8d779aabe781e84e7a.jpg', 'admin', '::1', '2015-08-05 10:39:52');
INSERT INTO `admin_nqt_logs` VALUES ('33', 'companies', '1', 'company_name', 'Update', 'JV-IT', 'DELL', 'admin', '::1', '2015-08-05 10:41:57');
INSERT INTO `admin_nqt_logs` VALUES ('34', 'companies', '1', 'service_name', 'Update', 'J-SaiGon', 'DELL', 'admin', '::1', '2015-08-05 10:41:57');
INSERT INTO `admin_nqt_logs` VALUES ('35', 'companies', '2', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-08-05 10:42:17');
INSERT INTO `admin_nqt_logs` VALUES ('36', 'companies', '3', 'Add new', 'Add new', '', '', 'admin', '127.0.0.1', '2015-08-23 10:52:36');
INSERT INTO `admin_nqt_logs` VALUES ('37', 'members', '60', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-08-24 10:11:27');
INSERT INTO `admin_nqt_logs` VALUES ('38', 'companies', '19', 'full_name', 'Update', 'kimcuu', 'ミタニ', 'admin', '::1', '2015-09-07 13:14:24');
INSERT INTO `admin_nqt_logs` VALUES ('39', 'companies', '19', 'katakana', 'Update', 'kimcuu', 'ミタニ', 'admin', '::1', '2015-09-07 13:14:24');
INSERT INTO `admin_nqt_logs` VALUES ('40', 'companies', '19', 'company_name', 'Update', 'kimcuu', 'ミタニ', 'admin', '::1', '2015-09-07 13:14:24');
INSERT INTO `admin_nqt_logs` VALUES ('41', 'companies', '19', 'service_name', 'Update', 'kimcuu', 'ミタニ', 'admin', '::1', '2015-09-07 13:14:24');
INSERT INTO `admin_nqt_logs` VALUES ('42', 'companies', '19', 'logo', 'Update', null, '2015/09/65a4de6598031a488c8b31fd26058d0f.jpg', 'admin', '::1', '2015-09-07 13:14:24');
INSERT INTO `admin_nqt_logs` VALUES ('43', 'companies', '1', 'logo', 'Update', '2015/08/b42aa98d5dc41e8d779aabe781e84e7a.jpg', '2015/09/96564b625317385f6f006d913e49b093.png', 'admin', '::1', '2015-09-11 16:08:16');
INSERT INTO `admin_nqt_logs` VALUES ('44', 'companies', '60', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-11 16:43:36');
INSERT INTO `admin_nqt_logs` VALUES ('45', 'companies', '60', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-11 16:43:36');
INSERT INTO `admin_nqt_logs` VALUES ('46', 'companies', '2', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-11 16:43:50');
INSERT INTO `admin_nqt_logs` VALUES ('47', 'companies', '1', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-11 16:43:52');
INSERT INTO `admin_nqt_logs` VALUES ('48', 'companies', '2', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-11 16:44:33');
INSERT INTO `admin_nqt_logs` VALUES ('49', 'companies', '2', 'end_date', 'Update', null, '2015-09-04 00:00:00', 'admin', '::1', '2015-09-14 09:51:14');
INSERT INTO `admin_nqt_logs` VALUES ('50', 'companies', '2', 'end_date', 'Update', '2015-09-04 00:00:00', '2015-09-14 00:00:00', 'admin', '::1', '2015-09-14 09:59:05');
INSERT INTO `admin_nqt_logs` VALUES ('51', 'companies', '2', 'end_date', 'Update', '2015-09-14 00:00:00', '1970-01-01 08:00:00', 'admin', '::1', '2015-09-14 10:13:51');
INSERT INTO `admin_nqt_logs` VALUES ('52', 'companies', '2', 'end_date', 'Update', '1970-01-01 08:00:00', 'NULL', 'admin', '::1', '2015-09-14 10:20:59');
INSERT INTO `admin_nqt_logs` VALUES ('53', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', 'NULL', 'admin', '::1', '2015-09-14 10:23:39');
INSERT INTO `admin_nqt_logs` VALUES ('54', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', 'Null', 'admin', '::1', '2015-09-14 10:24:46');
INSERT INTO `admin_nqt_logs` VALUES ('55', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', 'Null', 'admin', '::1', '2015-09-14 10:24:55');
INSERT INTO `admin_nqt_logs` VALUES ('56', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', '2015-09-24 00:00:00', 'admin', '::1', '2015-09-14 10:26:39');
INSERT INTO `admin_nqt_logs` VALUES ('57', 'companies', '2', 'end_date', 'Update', '2015-09-24 00:00:00', 'Null', 'admin', '::1', '2015-09-14 10:26:51');
INSERT INTO `admin_nqt_logs` VALUES ('58', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', 'Null', 'admin', '::1', '2015-09-14 10:28:51');
INSERT INTO `admin_nqt_logs` VALUES ('59', 'members', '106', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 10:41:53');
INSERT INTO `admin_nqt_logs` VALUES ('60', 'companies', '2', 'order', 'Update', '2', '4', 'admin', '::1', '2015-09-14 10:54:07');
INSERT INTO `admin_nqt_logs` VALUES ('61', 'companies', '2', 'end_date', 'Update', '0000-00-00 00:00:00', '1970-01-01 08:00:00', 'admin', '::1', '2015-09-14 10:54:07');
INSERT INTO `admin_nqt_logs` VALUES ('62', 'companies', '61', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 10:57:17');
INSERT INTO `admin_nqt_logs` VALUES ('63', 'companies', '62', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 10:57:17');
INSERT INTO `admin_nqt_logs` VALUES ('64', 'companies', '61', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 10:58:06');
INSERT INTO `admin_nqt_logs` VALUES ('65', 'members', '107', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 11:07:16');
INSERT INTO `admin_nqt_logs` VALUES ('66', 'companies', '62', 'end_date', 'Update', '0000-00-00 00:00:00', null, 'admin', '::1', '2015-09-14 11:24:27');
INSERT INTO `admin_nqt_logs` VALUES ('67', 'companies', '2', 'end_date', 'Update', '1970-01-01 08:00:00', '2015-09-18 00:00:00', 'admin', '::1', '2015-09-14 11:24:53');
INSERT INTO `admin_nqt_logs` VALUES ('68', 'companies', '2', 'end_date', 'Update', '2015-09-18 00:00:00', null, 'admin', '::1', '2015-09-14 11:25:04');
INSERT INTO `admin_nqt_logs` VALUES ('69', 'companies', '60', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-14 13:12:54');
INSERT INTO `admin_nqt_logs` VALUES ('70', 'companies', '2', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-14 13:12:55');
INSERT INTO `admin_nqt_logs` VALUES ('71', 'companies', '1', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-14 13:12:59');
INSERT INTO `admin_nqt_logs` VALUES ('72', 'companies', '63', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:39:21');
INSERT INTO `admin_nqt_logs` VALUES ('73', 'companies', '64', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:43:05');
INSERT INTO `admin_nqt_logs` VALUES ('74', 'companies', '64', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:43:32');
INSERT INTO `admin_nqt_logs` VALUES ('75', 'companies', '63', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:43:32');
INSERT INTO `admin_nqt_logs` VALUES ('76', 'companies', '65', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:44:27');
INSERT INTO `admin_nqt_logs` VALUES ('77', 'companies', '66', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:00');
INSERT INTO `admin_nqt_logs` VALUES ('78', 'companies', '67', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:06');
INSERT INTO `admin_nqt_logs` VALUES ('79', 'companies', '68', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:13');
INSERT INTO `admin_nqt_logs` VALUES ('80', 'companies', '69', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:18');
INSERT INTO `admin_nqt_logs` VALUES ('81', 'companies', '70', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:22');
INSERT INTO `admin_nqt_logs` VALUES ('82', 'companies', '71', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:45:28');
INSERT INTO `admin_nqt_logs` VALUES ('83', 'companies', '66', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('84', 'companies', '69', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('85', 'companies', '68', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('86', 'companies', '65', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('87', 'companies', '67', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-14 13:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('88', 'companies', '72', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:51:40');
INSERT INTO `admin_nqt_logs` VALUES ('89', 'companies', '73', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-14 13:56:51');
INSERT INTO `admin_nqt_logs` VALUES ('90', 'companies', '73', 'end_date', 'Update', null, '2015-09-30 00:00:00', 'admin', '::1', '2015-09-14 14:18:06');
INSERT INTO `admin_nqt_logs` VALUES ('91', 'companies', '73', 'start_date', 'Update', null, '2015-09-30 00:00:00', 'admin', '::1', '2015-09-14 14:18:06');
INSERT INTO `admin_nqt_logs` VALUES ('92', 'companies', '73', 'end_date', 'Update', '2015-09-30 00:00:00', null, 'admin', '::1', '2015-09-14 14:18:23');
INSERT INTO `admin_nqt_logs` VALUES ('93', 'companies', '73', 'start_date', 'Update', '2015-09-30 00:00:00', null, 'admin', '::1', '2015-09-14 14:18:23');
INSERT INTO `admin_nqt_logs` VALUES ('94', 'companies', '73', 'end_date', 'Update', null, '2015-09-15 00:00:00', 'admin', '::1', '2015-09-15 09:33:41');
INSERT INTO `admin_nqt_logs` VALUES ('95', 'companies', '73', 'start_date', 'Update', null, '2015-09-12 00:00:00', 'admin', '::1', '2015-09-15 09:33:42');
INSERT INTO `admin_nqt_logs` VALUES ('96', 'companies', '73', 'end_date', 'Update', '2015-09-15 00:00:00', null, 'admin', '::1', '2015-09-15 09:33:58');
INSERT INTO `admin_nqt_logs` VALUES ('97', 'companies', '73', 'start_date', 'Update', '2015-09-12 00:00:00', null, 'admin', '::1', '2015-09-15 09:33:58');
INSERT INTO `admin_nqt_logs` VALUES ('98', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:30');
INSERT INTO `admin_nqt_logs` VALUES ('99', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:31');
INSERT INTO `admin_nqt_logs` VALUES ('100', 'companies', '71', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:31');
INSERT INTO `admin_nqt_logs` VALUES ('101', 'companies', '70', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:32');
INSERT INTO `admin_nqt_logs` VALUES ('102', 'companies', '62', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:33');
INSERT INTO `admin_nqt_logs` VALUES ('103', 'companies', '60', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:34');
INSERT INTO `admin_nqt_logs` VALUES ('104', 'companies', '2', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:35');
INSERT INTO `admin_nqt_logs` VALUES ('105', 'companies', '1', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 09:53:36');
INSERT INTO `admin_nqt_logs` VALUES ('106', 'companies', '62', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 09:53:53');
INSERT INTO `admin_nqt_logs` VALUES ('107', 'companies', '2', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 09:53:56');
INSERT INTO `admin_nqt_logs` VALUES ('108', 'companies', '1', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 09:53:57');
INSERT INTO `admin_nqt_logs` VALUES ('109', 'companies', '73', 'end_date', 'Update', null, '2015-09-18 00:00:00', 'admin', '::1', '2015-09-15 10:12:27');
INSERT INTO `admin_nqt_logs` VALUES ('110', 'companies', '73', 'start_date', 'Update', null, '2015-09-11 00:00:00', 'admin', '::1', '2015-09-15 10:12:27');
INSERT INTO `admin_nqt_logs` VALUES ('111', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 10:56:15');
INSERT INTO `admin_nqt_logs` VALUES ('112', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 10:56:51');
INSERT INTO `admin_nqt_logs` VALUES ('113', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 10:56:52');
INSERT INTO `admin_nqt_logs` VALUES ('114', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 10:56:57');
INSERT INTO `admin_nqt_logs` VALUES ('115', 'companies', '71', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 10:57:00');
INSERT INTO `admin_nqt_logs` VALUES ('116', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 10:58:53');
INSERT INTO `admin_nqt_logs` VALUES ('117', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 10:58:55');
INSERT INTO `admin_nqt_logs` VALUES ('118', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 10:58:56');
INSERT INTO `admin_nqt_logs` VALUES ('119', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:00:52');
INSERT INTO `admin_nqt_logs` VALUES ('120', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:00:55');
INSERT INTO `admin_nqt_logs` VALUES ('121', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:00:55');
INSERT INTO `admin_nqt_logs` VALUES ('122', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:00:56');
INSERT INTO `admin_nqt_logs` VALUES ('123', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:00:57');
INSERT INTO `admin_nqt_logs` VALUES ('124', 'companies', '70', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:22');
INSERT INTO `admin_nqt_logs` VALUES ('125', 'companies', '70', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:23');
INSERT INTO `admin_nqt_logs` VALUES ('126', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:31');
INSERT INTO `admin_nqt_logs` VALUES ('127', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:32');
INSERT INTO `admin_nqt_logs` VALUES ('128', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:33');
INSERT INTO `admin_nqt_logs` VALUES ('129', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:35');
INSERT INTO `admin_nqt_logs` VALUES ('130', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:36');
INSERT INTO `admin_nqt_logs` VALUES ('131', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:37');
INSERT INTO `admin_nqt_logs` VALUES ('132', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:38');
INSERT INTO `admin_nqt_logs` VALUES ('133', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:39');
INSERT INTO `admin_nqt_logs` VALUES ('134', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:39');
INSERT INTO `admin_nqt_logs` VALUES ('135', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:42');
INSERT INTO `admin_nqt_logs` VALUES ('136', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:43');
INSERT INTO `admin_nqt_logs` VALUES ('137', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:43');
INSERT INTO `admin_nqt_logs` VALUES ('138', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:44');
INSERT INTO `admin_nqt_logs` VALUES ('139', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:10:47');
INSERT INTO `admin_nqt_logs` VALUES ('140', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:48');
INSERT INTO `admin_nqt_logs` VALUES ('141', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:10:49');
INSERT INTO `admin_nqt_logs` VALUES ('142', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:18:46');
INSERT INTO `admin_nqt_logs` VALUES ('143', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:18:47');
INSERT INTO `admin_nqt_logs` VALUES ('144', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:18:48');
INSERT INTO `admin_nqt_logs` VALUES ('145', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:18:49');
INSERT INTO `admin_nqt_logs` VALUES ('146', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 11:18:50');
INSERT INTO `admin_nqt_logs` VALUES ('147', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:19:18');
INSERT INTO `admin_nqt_logs` VALUES ('148', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:19:21');
INSERT INTO `admin_nqt_logs` VALUES ('149', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:22');
INSERT INTO `admin_nqt_logs` VALUES ('150', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:23');
INSERT INTO `admin_nqt_logs` VALUES ('151', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:23');
INSERT INTO `admin_nqt_logs` VALUES ('152', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:23');
INSERT INTO `admin_nqt_logs` VALUES ('153', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:23');
INSERT INTO `admin_nqt_logs` VALUES ('154', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:23');
INSERT INTO `admin_nqt_logs` VALUES ('155', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:24');
INSERT INTO `admin_nqt_logs` VALUES ('156', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 11:20:24');
INSERT INTO `admin_nqt_logs` VALUES ('157', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:41');
INSERT INTO `admin_nqt_logs` VALUES ('158', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:42');
INSERT INTO `admin_nqt_logs` VALUES ('159', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:42');
INSERT INTO `admin_nqt_logs` VALUES ('160', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:42');
INSERT INTO `admin_nqt_logs` VALUES ('161', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:44');
INSERT INTO `admin_nqt_logs` VALUES ('162', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:22:45');
INSERT INTO `admin_nqt_logs` VALUES ('163', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:45');
INSERT INTO `admin_nqt_logs` VALUES ('164', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:22:46');
INSERT INTO `admin_nqt_logs` VALUES ('165', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:47');
INSERT INTO `admin_nqt_logs` VALUES ('166', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:22:48');
INSERT INTO `admin_nqt_logs` VALUES ('167', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:50');
INSERT INTO `admin_nqt_logs` VALUES ('168', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:22:51');
INSERT INTO `admin_nqt_logs` VALUES ('169', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:22:52');
INSERT INTO `admin_nqt_logs` VALUES ('170', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:25:47');
INSERT INTO `admin_nqt_logs` VALUES ('171', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:25:48');
INSERT INTO `admin_nqt_logs` VALUES ('172', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:25:48');
INSERT INTO `admin_nqt_logs` VALUES ('173', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:25:48');
INSERT INTO `admin_nqt_logs` VALUES ('174', 'companies', '2', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:26:06');
INSERT INTO `admin_nqt_logs` VALUES ('175', 'companies', '2', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:26:07');
INSERT INTO `admin_nqt_logs` VALUES ('176', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:26:09');
INSERT INTO `admin_nqt_logs` VALUES ('177', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:26:09');
INSERT INTO `admin_nqt_logs` VALUES ('178', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:26:12');
INSERT INTO `admin_nqt_logs` VALUES ('179', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:26:13');
INSERT INTO `admin_nqt_logs` VALUES ('180', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:26:14');
INSERT INTO `admin_nqt_logs` VALUES ('181', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:26:15');
INSERT INTO `admin_nqt_logs` VALUES ('182', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 12:26:28');
INSERT INTO `admin_nqt_logs` VALUES ('183', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 12:28:26');
INSERT INTO `admin_nqt_logs` VALUES ('184', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 12:28:28');
INSERT INTO `admin_nqt_logs` VALUES ('185', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 12:28:29');
INSERT INTO `admin_nqt_logs` VALUES ('186', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 12:28:29');
INSERT INTO `admin_nqt_logs` VALUES ('187', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:28:45');
INSERT INTO `admin_nqt_logs` VALUES ('188', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:28:57');
INSERT INTO `admin_nqt_logs` VALUES ('189', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:32:19');
INSERT INTO `admin_nqt_logs` VALUES ('190', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 12:32:19');
INSERT INTO `admin_nqt_logs` VALUES ('191', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:32:55');
INSERT INTO `admin_nqt_logs` VALUES ('192', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 12:33:09');
INSERT INTO `admin_nqt_logs` VALUES ('193', 'companies', null, 'status', 'update', null, '1', 'admin', '::1', '2015-09-15 13:00:08');
INSERT INTO `admin_nqt_logs` VALUES ('194', 'companies', null, 'forever', 'update', null, '1', 'admin', '::1', '2015-09-15 13:00:34');
INSERT INTO `admin_nqt_logs` VALUES ('195', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:02:11');
INSERT INTO `admin_nqt_logs` VALUES ('196', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:02:13');
INSERT INTO `admin_nqt_logs` VALUES ('197', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:02:14');
INSERT INTO `admin_nqt_logs` VALUES ('198', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:02:31');
INSERT INTO `admin_nqt_logs` VALUES ('199', 'companies', '60', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:02:45');
INSERT INTO `admin_nqt_logs` VALUES ('200', 'companies', '60', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:02:46');
INSERT INTO `admin_nqt_logs` VALUES ('201', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:02:49');
INSERT INTO `admin_nqt_logs` VALUES ('202', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:02:49');
INSERT INTO `admin_nqt_logs` VALUES ('203', 'companies', '72', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:03:14');
INSERT INTO `admin_nqt_logs` VALUES ('204', 'companies', '72', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:03:15');
INSERT INTO `admin_nqt_logs` VALUES ('205', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:03:54');
INSERT INTO `admin_nqt_logs` VALUES ('206', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:03:56');
INSERT INTO `admin_nqt_logs` VALUES ('207', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:53');
INSERT INTO `admin_nqt_logs` VALUES ('208', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:53');
INSERT INTO `admin_nqt_logs` VALUES ('209', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:54');
INSERT INTO `admin_nqt_logs` VALUES ('210', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:54');
INSERT INTO `admin_nqt_logs` VALUES ('211', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:54');
INSERT INTO `admin_nqt_logs` VALUES ('212', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:54');
INSERT INTO `admin_nqt_logs` VALUES ('213', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:54');
INSERT INTO `admin_nqt_logs` VALUES ('214', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:55');
INSERT INTO `admin_nqt_logs` VALUES ('215', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:55');
INSERT INTO `admin_nqt_logs` VALUES ('216', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:55');
INSERT INTO `admin_nqt_logs` VALUES ('217', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:55');
INSERT INTO `admin_nqt_logs` VALUES ('218', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:56');
INSERT INTO `admin_nqt_logs` VALUES ('219', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:56');
INSERT INTO `admin_nqt_logs` VALUES ('220', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:56');
INSERT INTO `admin_nqt_logs` VALUES ('221', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:57');
INSERT INTO `admin_nqt_logs` VALUES ('222', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:57');
INSERT INTO `admin_nqt_logs` VALUES ('223', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('224', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('225', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('226', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('227', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('228', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:05:58');
INSERT INTO `admin_nqt_logs` VALUES ('229', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:05:59');
INSERT INTO `admin_nqt_logs` VALUES ('230', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:00');
INSERT INTO `admin_nqt_logs` VALUES ('231', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:00');
INSERT INTO `admin_nqt_logs` VALUES ('232', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:02');
INSERT INTO `admin_nqt_logs` VALUES ('233', 'companies', '73', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:03');
INSERT INTO `admin_nqt_logs` VALUES ('234', 'companies', '73', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:04');
INSERT INTO `admin_nqt_logs` VALUES ('235', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:08');
INSERT INTO `admin_nqt_logs` VALUES ('236', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:09');
INSERT INTO `admin_nqt_logs` VALUES ('237', 'companies', '73', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:10');
INSERT INTO `admin_nqt_logs` VALUES ('238', 'companies', '73', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:11');
INSERT INTO `admin_nqt_logs` VALUES ('239', 'companies', '72', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:16');
INSERT INTO `admin_nqt_logs` VALUES ('240', 'companies', '72', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:17');
INSERT INTO `admin_nqt_logs` VALUES ('241', 'companies', '72', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:18');
INSERT INTO `admin_nqt_logs` VALUES ('242', 'companies', '72', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:06:19');
INSERT INTO `admin_nqt_logs` VALUES ('243', 'companies', '72', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:06:20');
INSERT INTO `admin_nqt_logs` VALUES ('244', 'companies', '71', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:07:08');
INSERT INTO `admin_nqt_logs` VALUES ('245', 'companies', '71', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:07:08');
INSERT INTO `admin_nqt_logs` VALUES ('246', 'companies', '71', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:07:10');
INSERT INTO `admin_nqt_logs` VALUES ('247', 'companies', '74', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-15 13:08:38');
INSERT INTO `admin_nqt_logs` VALUES ('248', 'companies', '74', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:08:48');
INSERT INTO `admin_nqt_logs` VALUES ('249', 'companies', '74', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:08:49');
INSERT INTO `admin_nqt_logs` VALUES ('250', 'companies', '73', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('251', 'companies', '62', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('252', 'companies', '71', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('253', 'companies', '60', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('254', 'companies', '70', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('255', 'companies', '72', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('256', 'companies', '74', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-15 13:09:24');
INSERT INTO `admin_nqt_logs` VALUES ('257', 'companies', '2', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 13:13:55');
INSERT INTO `admin_nqt_logs` VALUES ('258', 'companies', '2', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 13:14:15');
INSERT INTO `admin_nqt_logs` VALUES ('259', 'companies', '75', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-15 13:14:56');
INSERT INTO `admin_nqt_logs` VALUES ('260', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 13:15:14');
INSERT INTO `admin_nqt_logs` VALUES ('261', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 13:52:43');
INSERT INTO `admin_nqt_logs` VALUES ('262', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 14:13:08');
INSERT INTO `admin_nqt_logs` VALUES ('263', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 14:13:20');
INSERT INTO `admin_nqt_logs` VALUES ('264', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 14:13:29');
INSERT INTO `admin_nqt_logs` VALUES ('265', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 14:15:52');
INSERT INTO `admin_nqt_logs` VALUES ('266', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 14:16:07');
INSERT INTO `admin_nqt_logs` VALUES ('267', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 14:17:02');
INSERT INTO `admin_nqt_logs` VALUES ('268', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 14:19:24');
INSERT INTO `admin_nqt_logs` VALUES ('269', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 14:20:46');
INSERT INTO `admin_nqt_logs` VALUES ('270', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 14:56:40');
INSERT INTO `admin_nqt_logs` VALUES ('271', 'companies', '75', 'end_date', 'Update', '2015-09-12 00:00:00', null, 'admin', '::1', '2015-09-15 14:56:40');
INSERT INTO `admin_nqt_logs` VALUES ('272', 'companies', '75', 'start_date', 'Update', '2015-09-05 00:00:00', null, 'admin', '::1', '2015-09-15 14:56:40');
INSERT INTO `admin_nqt_logs` VALUES ('273', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 14:56:52');
INSERT INTO `admin_nqt_logs` VALUES ('274', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:00:11');
INSERT INTO `admin_nqt_logs` VALUES ('275', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:00:41');
INSERT INTO `admin_nqt_logs` VALUES ('276', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:04:18');
INSERT INTO `admin_nqt_logs` VALUES ('277', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:04:28');
INSERT INTO `admin_nqt_logs` VALUES ('278', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:05:23');
INSERT INTO `admin_nqt_logs` VALUES ('279', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:07:02');
INSERT INTO `admin_nqt_logs` VALUES ('280', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:08:45');
INSERT INTO `admin_nqt_logs` VALUES ('281', 'companies', '75', 'end_date', 'Update', null, '2015-10-08 00:00:00', 'admin', '::1', '2015-09-15 15:20:38');
INSERT INTO `admin_nqt_logs` VALUES ('282', 'companies', '75', 'start_date', 'Update', null, '2015-10-06 00:00:00', 'admin', '::1', '2015-09-15 15:20:38');
INSERT INTO `admin_nqt_logs` VALUES ('283', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:21:29');
INSERT INTO `admin_nqt_logs` VALUES ('284', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:21:49');
INSERT INTO `admin_nqt_logs` VALUES ('285', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:21:53');
INSERT INTO `admin_nqt_logs` VALUES ('286', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:22:34');
INSERT INTO `admin_nqt_logs` VALUES ('287', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:25:24');
INSERT INTO `admin_nqt_logs` VALUES ('288', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:25:37');
INSERT INTO `admin_nqt_logs` VALUES ('289', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:25:41');
INSERT INTO `admin_nqt_logs` VALUES ('290', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:26:27');
INSERT INTO `admin_nqt_logs` VALUES ('291', 'companies', '75', 'end_date', 'Update', '2015-10-08 00:00:00', null, 'admin', '::1', '2015-09-15 15:26:34');
INSERT INTO `admin_nqt_logs` VALUES ('292', 'companies', '75', 'start_date', 'Update', '2015-10-06 00:00:00', null, 'admin', '::1', '2015-09-15 15:26:34');
INSERT INTO `admin_nqt_logs` VALUES ('293', 'companies', '75', 'end_date', 'Update', null, '2015-10-29 00:00:00', 'admin', '::1', '2015-09-15 15:27:41');
INSERT INTO `admin_nqt_logs` VALUES ('294', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:30:05');
INSERT INTO `admin_nqt_logs` VALUES ('295', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:30:27');
INSERT INTO `admin_nqt_logs` VALUES ('296', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:32:18');
INSERT INTO `admin_nqt_logs` VALUES ('297', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:32:45');
INSERT INTO `admin_nqt_logs` VALUES ('298', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:33:00');
INSERT INTO `admin_nqt_logs` VALUES ('299', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:46:32');
INSERT INTO `admin_nqt_logs` VALUES ('300', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:47:10');
INSERT INTO `admin_nqt_logs` VALUES ('301', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:47:58');
INSERT INTO `admin_nqt_logs` VALUES ('302', 'companies', '75', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 15:48:51');
INSERT INTO `admin_nqt_logs` VALUES ('303', 'companies', '75', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 15:54:08');
INSERT INTO `admin_nqt_logs` VALUES ('304', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 16:04:48');
INSERT INTO `admin_nqt_logs` VALUES ('305', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 16:06:12');
INSERT INTO `admin_nqt_logs` VALUES ('306', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 16:06:16');
INSERT INTO `admin_nqt_logs` VALUES ('307', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 16:06:23');
INSERT INTO `admin_nqt_logs` VALUES ('308', 'companies', '75', 'end_date', 'Update', '2015-10-29 00:00:00', '2015-11-07 00:00:00', 'admin', '::1', '2015-09-15 16:15:33');
INSERT INTO `admin_nqt_logs` VALUES ('309', 'companies', '75', 'start_date', 'Update', null, '2015-09-30 00:00:00', 'admin', '::1', '2015-09-15 16:15:33');
INSERT INTO `admin_nqt_logs` VALUES ('310', 'companies', '75', 'end_date', 'Update', '2015-11-07 00:00:00', null, 'admin', '::1', '2015-09-15 16:26:28');
INSERT INTO `admin_nqt_logs` VALUES ('311', 'companies', '75', 'start_date', 'Update', '2015-09-30 00:00:00', null, 'admin', '::1', '2015-09-15 16:26:28');
INSERT INTO `admin_nqt_logs` VALUES ('312', 'companies', '75', 'end_date', 'Update', null, '2015-11-19 00:00:00', 'admin', '::1', '2015-09-15 16:30:06');
INSERT INTO `admin_nqt_logs` VALUES ('313', 'companies', '75', 'start_date', 'Update', null, '2015-09-08 00:00:00', 'admin', '::1', '2015-09-15 16:30:06');
INSERT INTO `admin_nqt_logs` VALUES ('314', 'companies', '75', 'end_date', 'Update', '2015-11-19 00:00:00', null, 'admin', '::1', '2015-09-15 16:31:16');
INSERT INTO `admin_nqt_logs` VALUES ('315', 'companies', '75', 'start_date', 'Update', '2015-09-08 00:00:00', null, 'admin', '::1', '2015-09-15 16:31:16');
INSERT INTO `admin_nqt_logs` VALUES ('316', 'companies', '76', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-15 16:41:27');
INSERT INTO `admin_nqt_logs` VALUES ('317', 'companies', '76', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 16:46:35');
INSERT INTO `admin_nqt_logs` VALUES ('318', 'companies', '76', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-15 16:46:50');
INSERT INTO `admin_nqt_logs` VALUES ('319', 'companies', '76', 'end_date', 'Update', null, '2015-09-11 00:00:00', 'admin', '::1', '2015-09-15 16:47:34');
INSERT INTO `admin_nqt_logs` VALUES ('320', 'companies', '76', 'start_date', 'Update', null, '2015-09-01 00:00:00', 'admin', '::1', '2015-09-15 16:47:34');
INSERT INTO `admin_nqt_logs` VALUES ('321', 'companies', '76', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 16:49:15');
INSERT INTO `admin_nqt_logs` VALUES ('322', 'companies', '76', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 16:49:23');
INSERT INTO `admin_nqt_logs` VALUES ('323', 'companies', '76', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-15 16:49:47');
INSERT INTO `admin_nqt_logs` VALUES ('324', 'companies', '76', 'end_date', 'Update', '2015-09-11 00:00:00', null, 'admin', '::1', '2015-09-15 16:49:47');
INSERT INTO `admin_nqt_logs` VALUES ('325', 'companies', '76', 'start_date', 'Update', '2015-09-01 00:00:00', null, 'admin', '::1', '2015-09-15 16:49:47');
INSERT INTO `admin_nqt_logs` VALUES ('326', 'companies', '76', 'forever', 'Update', '1', null, 'admin', '::1', '2015-09-15 16:50:10');
INSERT INTO `admin_nqt_logs` VALUES ('327', 'companies', '76', 'end_date', 'Update', null, '2015-10-03 00:00:00', 'admin', '::1', '2015-09-15 16:50:10');
INSERT INTO `admin_nqt_logs` VALUES ('328', 'companies', '76', 'start_date', 'Update', null, '2015-09-18 00:00:00', 'admin', '::1', '2015-09-15 16:50:10');
INSERT INTO `admin_nqt_logs` VALUES ('329', 'companies', '76', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:46');
INSERT INTO `admin_nqt_logs` VALUES ('330', 'companies', '76', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:47');
INSERT INTO `admin_nqt_logs` VALUES ('331', 'companies', '76', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 17:00:49');
INSERT INTO `admin_nqt_logs` VALUES ('332', 'companies', '76', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:52');
INSERT INTO `admin_nqt_logs` VALUES ('333', 'companies', '76', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 17:00:52');
INSERT INTO `admin_nqt_logs` VALUES ('334', 'companies', '75', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:53');
INSERT INTO `admin_nqt_logs` VALUES ('335', 'companies', '75', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 17:00:54');
INSERT INTO `admin_nqt_logs` VALUES ('336', 'companies', '2', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:55');
INSERT INTO `admin_nqt_logs` VALUES ('337', 'companies', '2', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 17:00:56');
INSERT INTO `admin_nqt_logs` VALUES ('338', 'companies', '76', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-15 17:00:59');
INSERT INTO `admin_nqt_logs` VALUES ('339', 'companies', '76', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-15 17:00:59');
INSERT INTO `admin_nqt_logs` VALUES ('340', 'admincp_accounts', '21', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-15 17:24:15');
INSERT INTO `admin_nqt_logs` VALUES ('341', 'members', '107', 'status', 'update', '1', '0', 'kimcuu1992', '::1', '2015-09-15 17:24:40');
INSERT INTO `admin_nqt_logs` VALUES ('342', 'members', '107', 'status', 'update', '0', '1', 'kimcuu1992', '::1', '2015-09-15 17:24:40');
INSERT INTO `admin_nqt_logs` VALUES ('343', 'job_recruiments', '142', 'status', 'update', '1', '0', 'kimcuu1992', '::1', '2015-09-15 18:00:48');
INSERT INTO `admin_nqt_logs` VALUES ('344', 'job_recruiments', '142', 'status', 'update', '0', '1', 'kimcuu1992', '::1', '2015-09-15 18:00:49');
INSERT INTO `admin_nqt_logs` VALUES ('345', 'members', '100', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 14:19:36');
INSERT INTO `admin_nqt_logs` VALUES ('346', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 14:33:42');
INSERT INTO `admin_nqt_logs` VALUES ('347', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 14:34:28');
INSERT INTO `admin_nqt_logs` VALUES ('348', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 14:34:29');
INSERT INTO `admin_nqt_logs` VALUES ('349', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 14:34:30');
INSERT INTO `admin_nqt_logs` VALUES ('350', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 14:34:32');
INSERT INTO `admin_nqt_logs` VALUES ('351', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 14:34:58');
INSERT INTO `admin_nqt_logs` VALUES ('352', 'companies', '78', 'full_name', 'Update', 'kimcuu', '三谷ミタニ', 'admin', '::1', '2015-09-16 14:45:32');
INSERT INTO `admin_nqt_logs` VALUES ('353', 'companies', '78', 'katakana', 'Update', 'kimcuu', 'ミタニ', 'admin', '::1', '2015-09-16 14:45:32');
INSERT INTO `admin_nqt_logs` VALUES ('354', 'companies', '78', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-16 14:45:32');
INSERT INTO `admin_nqt_logs` VALUES ('355', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:02:41');
INSERT INTO `admin_nqt_logs` VALUES ('356', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:02:41');
INSERT INTO `admin_nqt_logs` VALUES ('357', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:20:35');
INSERT INTO `admin_nqt_logs` VALUES ('358', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:20:36');
INSERT INTO `admin_nqt_logs` VALUES ('359', 'companies', '77', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:20:36');
INSERT INTO `admin_nqt_logs` VALUES ('360', 'companies', '76', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:20:38');
INSERT INTO `admin_nqt_logs` VALUES ('361', 'companies', '78', 'first', 'Update', '1', null, 'admin', '::1', '2015-09-16 15:21:06');
INSERT INTO `admin_nqt_logs` VALUES ('362', 'companies', '77', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:03');
INSERT INTO `admin_nqt_logs` VALUES ('363', 'companies', '77', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:03');
INSERT INTO `admin_nqt_logs` VALUES ('364', 'companies', '78', 'first', 'Update', null, '1', 'admin', '::1', '2015-09-16 15:23:16');
INSERT INTO `admin_nqt_logs` VALUES ('365', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:20');
INSERT INTO `admin_nqt_logs` VALUES ('366', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:21');
INSERT INTO `admin_nqt_logs` VALUES ('367', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:21');
INSERT INTO `admin_nqt_logs` VALUES ('368', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:22');
INSERT INTO `admin_nqt_logs` VALUES ('369', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:23');
INSERT INTO `admin_nqt_logs` VALUES ('370', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:23');
INSERT INTO `admin_nqt_logs` VALUES ('371', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:24');
INSERT INTO `admin_nqt_logs` VALUES ('372', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:25');
INSERT INTO `admin_nqt_logs` VALUES ('373', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:26');
INSERT INTO `admin_nqt_logs` VALUES ('374', 'companies', '78', 'first', 'Update', '0', null, 'admin', '::1', '2015-09-16 15:23:43');
INSERT INTO `admin_nqt_logs` VALUES ('375', 'companies', '77', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:50');
INSERT INTO `admin_nqt_logs` VALUES ('376', 'companies', '77', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:51');
INSERT INTO `admin_nqt_logs` VALUES ('377', 'companies', '77', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:51');
INSERT INTO `admin_nqt_logs` VALUES ('378', 'companies', '77', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:52');
INSERT INTO `admin_nqt_logs` VALUES ('379', 'companies', '75', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:23:53');
INSERT INTO `admin_nqt_logs` VALUES ('380', 'companies', '75', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:23:54');
INSERT INTO `admin_nqt_logs` VALUES ('381', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:24:33');
INSERT INTO `admin_nqt_logs` VALUES ('382', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:24:33');
INSERT INTO `admin_nqt_logs` VALUES ('383', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:24:34');
INSERT INTO `admin_nqt_logs` VALUES ('384', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:24:35');
INSERT INTO `admin_nqt_logs` VALUES ('385', 'companies', '78', 'forever', 'Update', '1', null, 'admin', '::1', '2015-09-16 15:27:12');
INSERT INTO `admin_nqt_logs` VALUES ('386', 'companies', '78', 'end_date', 'Update', null, '2015-10-10 00:00:00', 'admin', '::1', '2015-09-16 15:27:12');
INSERT INTO `admin_nqt_logs` VALUES ('387', 'companies', '78', 'start_date', 'Update', null, '2015-09-03 00:00:00', 'admin', '::1', '2015-09-16 15:27:12');
INSERT INTO `admin_nqt_logs` VALUES ('388', 'companies', '78', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-16 15:28:21');
INSERT INTO `admin_nqt_logs` VALUES ('389', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:28:38');
INSERT INTO `admin_nqt_logs` VALUES ('390', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:28:39');
INSERT INTO `admin_nqt_logs` VALUES ('391', 'companies', '78', 'forever', 'Update', null, '1', 'admin', '::1', '2015-09-16 15:28:46');
INSERT INTO `admin_nqt_logs` VALUES ('392', 'companies', '78', 'first', 'Update', '1', '0', 'admin', '::1', '2015-09-16 15:28:46');
INSERT INTO `admin_nqt_logs` VALUES ('393', 'companies', '78', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-16 15:29:04');
INSERT INTO `admin_nqt_logs` VALUES ('394', 'companies', '78', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 15:29:09');
INSERT INTO `admin_nqt_logs` VALUES ('395', 'companies', '78', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 15:29:10');
INSERT INTO `admin_nqt_logs` VALUES ('396', 'companies', '78', 'end_date', 'Update', '2015-10-10 00:00:00', null, 'admin', '::1', '2015-09-16 15:30:11');
INSERT INTO `admin_nqt_logs` VALUES ('397', 'companies', '78', 'start_date', 'Update', '2015-09-03 00:00:00', null, 'admin', '::1', '2015-09-16 15:30:11');
INSERT INTO `admin_nqt_logs` VALUES ('398', 'companies', '78', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-16 15:30:46');
INSERT INTO `admin_nqt_logs` VALUES ('399', 'companies', '78', 'end_date', 'Update', null, '2015-10-17 00:00:00', 'admin', '::1', '2015-09-16 15:30:46');
INSERT INTO `admin_nqt_logs` VALUES ('400', 'companies', '78', 'start_date', 'Update', null, '2015-09-01 00:00:00', 'admin', '::1', '2015-09-16 15:30:46');
INSERT INTO `admin_nqt_logs` VALUES ('401', 'companies', '78', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-16 15:31:16');
INSERT INTO `admin_nqt_logs` VALUES ('402', 'companies', '78', 'end_date', 'Update', '2015-10-17 00:00:00', null, 'admin', '::1', '2015-09-16 15:31:16');
INSERT INTO `admin_nqt_logs` VALUES ('403', 'companies', '78', 'start_date', 'Update', '2015-09-01 00:00:00', null, 'admin', '::1', '2015-09-16 15:31:16');
INSERT INTO `admin_nqt_logs` VALUES ('404', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:24:41');
INSERT INTO `admin_nqt_logs` VALUES ('405', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:24:41');
INSERT INTO `admin_nqt_logs` VALUES ('406', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:24:42');
INSERT INTO `admin_nqt_logs` VALUES ('407', 'companies', '78', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:24:43');
INSERT INTO `admin_nqt_logs` VALUES ('408', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:24:44');
INSERT INTO `admin_nqt_logs` VALUES ('409', 'companies', '78', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:24:44');
INSERT INTO `admin_nqt_logs` VALUES ('410', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:25:57');
INSERT INTO `admin_nqt_logs` VALUES ('411', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:25:58');
INSERT INTO `admin_nqt_logs` VALUES ('412', 'companies', '78', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:25:59');
INSERT INTO `admin_nqt_logs` VALUES ('413', 'companies', '78', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:26:00');
INSERT INTO `admin_nqt_logs` VALUES ('414', 'companies', '78', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:26:01');
INSERT INTO `admin_nqt_logs` VALUES ('415', 'companies', '77', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('416', 'companies', '78', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('417', 'companies', '1', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('418', 'companies', '75', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('419', 'companies', '76', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('420', 'companies', '2', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:31:44');
INSERT INTO `admin_nqt_logs` VALUES ('421', 'members', '107', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('422', 'members', '106', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('423', 'members', '105', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('424', 'members', '104', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('425', 'members', '103', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('426', 'members', '102', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('427', 'members', '99', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('428', 'members', '101', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('429', 'members', '97', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('430', 'members', '98', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:51');
INSERT INTO `admin_nqt_logs` VALUES ('431', 'members', '96', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:57');
INSERT INTO `admin_nqt_logs` VALUES ('432', 'members', '69', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:57');
INSERT INTO `admin_nqt_logs` VALUES ('433', 'members', '95', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:57');
INSERT INTO `admin_nqt_logs` VALUES ('434', 'members', '80', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:57');
INSERT INTO `admin_nqt_logs` VALUES ('435', 'members', '1', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:34:57');
INSERT INTO `admin_nqt_logs` VALUES ('436', 'members', '108', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-16 16:36:54');
INSERT INTO `admin_nqt_logs` VALUES ('437', 'companies', '79', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-16 16:38:32');
INSERT INTO `admin_nqt_logs` VALUES ('438', 'companies', '79', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:38:37');
INSERT INTO `admin_nqt_logs` VALUES ('439', 'companies', '79', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 16:38:38');
INSERT INTO `admin_nqt_logs` VALUES ('440', 'companies', '79', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-16 16:39:43');
INSERT INTO `admin_nqt_logs` VALUES ('441', 'companies', '79', 'first', 'Update', '1', '0', 'admin', '::1', '2015-09-16 16:39:59');
INSERT INTO `admin_nqt_logs` VALUES ('442', 'companies', '79', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-16 16:40:07');
INSERT INTO `admin_nqt_logs` VALUES ('443', 'companies', '79', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-16 16:45:11');
INSERT INTO `admin_nqt_logs` VALUES ('444', 'companies', '80', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-16 16:45:42');
INSERT INTO `admin_nqt_logs` VALUES ('445', 'companies', '80', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-16 20:40:19');
INSERT INTO `admin_nqt_logs` VALUES ('446', 'companies', '80', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-16 20:40:28');
INSERT INTO `admin_nqt_logs` VALUES ('447', 'companies', '80', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-16 20:40:35');
INSERT INTO `admin_nqt_logs` VALUES ('448', 'companies', '81', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-17 08:47:36');
INSERT INTO `admin_nqt_logs` VALUES ('449', 'companies', '80', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-17 08:47:37');
INSERT INTO `admin_nqt_logs` VALUES ('450', 'companies', '81', 'full_name', 'Update', 'aaa', '三谷ミタニ', 'admin', '::1', '2015-09-17 08:49:10');
INSERT INTO `admin_nqt_logs` VALUES ('451', 'companies', '81', 'katakana', 'Update', 'bbbb', 'ミタニ', 'admin', '::1', '2015-09-17 08:49:10');
INSERT INTO `admin_nqt_logs` VALUES ('452', 'companies', '81', 'logo', 'Update', null, '2015/09/2623f0db54589f815770608e17f3d129.png', 'admin', '::1', '2015-09-17 08:49:10');
INSERT INTO `admin_nqt_logs` VALUES ('453', 'companies', '81', 'status', 'Update', '0', '1', 'admin', '::1', '2015-09-17 08:49:10');
INSERT INTO `admin_nqt_logs` VALUES ('454', 'companies', '81', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-17 08:49:10');
INSERT INTO `admin_nqt_logs` VALUES ('455', 'companies', '81', 'logo', 'Update', '2015/09/2623f0db54589f815770608e17f3d129.png', '2015/09/130c525a409f8eb3f816ac57d1d436f3.png', 'admin', '::1', '2015-09-17 08:50:11');
INSERT INTO `admin_nqt_logs` VALUES ('456', 'companies', '82', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-17 09:08:23');
INSERT INTO `admin_nqt_logs` VALUES ('457', 'companies', '82', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-17 09:08:24');
INSERT INTO `admin_nqt_logs` VALUES ('458', 'members', '109', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-17 09:10:53');
INSERT INTO `admin_nqt_logs` VALUES ('459', 'companies', '81', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-17 09:49:44');
INSERT INTO `admin_nqt_logs` VALUES ('460', 'companies', '81', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-17 09:51:49');
INSERT INTO `admin_nqt_logs` VALUES ('461', 'companies', '81', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-17 16:39:02');
INSERT INTO `admin_nqt_logs` VALUES ('462', 'companies', '81', 'end_date', 'Update', null, '2015-09-01 00:00:00', 'admin', '::1', '2015-09-17 16:39:02');
INSERT INTO `admin_nqt_logs` VALUES ('463', 'companies', '81', 'start_date', 'Update', null, '2015-07-28 00:00:00', 'admin', '::1', '2015-09-17 16:39:02');
INSERT INTO `admin_nqt_logs` VALUES ('464', 'companies', '81', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-17 16:39:09');
INSERT INTO `admin_nqt_logs` VALUES ('465', 'companies', '81', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-17 16:39:13');
INSERT INTO `admin_nqt_logs` VALUES ('466', 'companies', '81', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-17 16:39:21');
INSERT INTO `admin_nqt_logs` VALUES ('467', 'companies', '81', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-17 16:40:32');
INSERT INTO `admin_nqt_logs` VALUES ('468', 'companies', '81', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-17 16:40:49');
INSERT INTO `admin_nqt_logs` VALUES ('469', 'companies', '81', 'end_date', 'Update', '2015-09-01 00:00:00', null, 'admin', '::1', '2015-09-17 16:40:49');
INSERT INTO `admin_nqt_logs` VALUES ('470', 'companies', '81', 'start_date', 'Update', '2015-07-28 00:00:00', null, 'admin', '::1', '2015-09-17 16:40:49');
INSERT INTO `admin_nqt_logs` VALUES ('471', 'companies', '81', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-17 16:41:03');
INSERT INTO `admin_nqt_logs` VALUES ('472', 'companies', '81', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-17 16:41:11');
INSERT INTO `admin_nqt_logs` VALUES ('473', 'companies', '82', 'full_name', 'Update', 'aaa', '三谷ミタニ', 'admin', '::1', '2015-09-17 17:11:48');
INSERT INTO `admin_nqt_logs` VALUES ('474', 'companies', '82', 'katakana', 'Update', 'bbbb', 'ミタニ', 'admin', '::1', '2015-09-17 17:11:48');
INSERT INTO `admin_nqt_logs` VALUES ('475', 'companies', '82', 'logo', 'Update', null, '2015/09/c09d9626def40779d720f68d8f9ef84d.png', 'admin', '::1', '2015-09-17 17:11:48');
INSERT INTO `admin_nqt_logs` VALUES ('476', 'companies', '82', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-17 17:11:48');
INSERT INTO `admin_nqt_logs` VALUES ('477', 'companies', '81', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-18 13:36:55');
INSERT INTO `admin_nqt_logs` VALUES ('478', 'companies', '80', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:03:36');
INSERT INTO `admin_nqt_logs` VALUES ('479', 'companies', '81', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:03:36');
INSERT INTO `admin_nqt_logs` VALUES ('480', 'companies', '82', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:03:36');
INSERT INTO `admin_nqt_logs` VALUES ('481', 'members', '112', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:04:51');
INSERT INTO `admin_nqt_logs` VALUES ('482', 'members', '111', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:04:51');
INSERT INTO `admin_nqt_logs` VALUES ('483', 'members', '109', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:04:51');
INSERT INTO `admin_nqt_logs` VALUES ('484', 'members', '108', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:04:51');
INSERT INTO `admin_nqt_logs` VALUES ('485', 'members', '110', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 14:04:51');
INSERT INTO `admin_nqt_logs` VALUES ('486', 'members', '113', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 15:02:21');
INSERT INTO `admin_nqt_logs` VALUES ('487', 'companies', '83', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 15:04:17');
INSERT INTO `admin_nqt_logs` VALUES ('488', 'companies', '83', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-18 15:06:03');
INSERT INTO `admin_nqt_logs` VALUES ('489', 'detail_member', '392', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:09');
INSERT INTO `admin_nqt_logs` VALUES ('490', 'detail_member', '393', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:14');
INSERT INTO `admin_nqt_logs` VALUES ('491', 'detail_member', '459', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:29');
INSERT INTO `admin_nqt_logs` VALUES ('492', 'detail_member', '396', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:29');
INSERT INTO `admin_nqt_logs` VALUES ('493', 'detail_member', '395', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:29');
INSERT INTO `admin_nqt_logs` VALUES ('494', 'detail_member', '394', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:30');
INSERT INTO `admin_nqt_logs` VALUES ('495', 'detail_member', '457', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:30');
INSERT INTO `admin_nqt_logs` VALUES ('496', 'detail_member', '458', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:09:30');
INSERT INTO `admin_nqt_logs` VALUES ('497', 'admincp_accounts', '21', 'permission', 'Update', '154|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd,203|4|r', '154|rwd,203|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd', 'admin', '::1', '2015-09-18 15:13:30');
INSERT INTO `admin_nqt_logs` VALUES ('498', 'companies', '83', 'first', 'Update', '1', '0', 'kimcuu1992', '::1', '2015-09-18 15:22:03');
INSERT INTO `admin_nqt_logs` VALUES ('499', 'companies', '83', 'first', 'Update', '0', '1', 'kimcuu1992', '::1', '2015-09-18 15:22:06');
INSERT INTO `admin_nqt_logs` VALUES ('500', 'admincp_accounts', '22', 'Add new', 'Add new', '', '', 'kimcuu1992', '::1', '2015-09-18 15:22:48');
INSERT INTO `admin_nqt_logs` VALUES ('501', 'admincp_accounts', '23', 'Add new', 'Add new', '', '', 'kimcuu', '::1', '2015-09-18 15:25:02');
INSERT INTO `admin_nqt_logs` VALUES ('502', 'admincp_account_groups', '1', 'permission', 'Update', '0|rwd,116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd,84|rwd,203|rwd', '0|rwd,154|rwd,203|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd', 'kimcuu', '::1', '2015-09-18 15:30:25');
INSERT INTO `admin_nqt_logs` VALUES ('503', 'admincp_accounts', '24', 'Add new', 'Add new', '', '', 'kimcuu', '::1', '2015-09-18 15:30:42');
INSERT INTO `admin_nqt_logs` VALUES ('504', 'admincp_accounts', '25', 'Add new', 'Add new', '', '', 'kimcuu', '::1', '2015-09-18 15:31:39');
INSERT INTO `admin_nqt_logs` VALUES ('505', 'admincp_accounts', '21', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:59:50');
INSERT INTO `admin_nqt_logs` VALUES ('506', 'admincp_accounts', '24', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:59:50');
INSERT INTO `admin_nqt_logs` VALUES ('507', 'admincp_accounts', '23', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:59:50');
INSERT INTO `admin_nqt_logs` VALUES ('508', 'admincp_accounts', '25', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:59:50');
INSERT INTO `admin_nqt_logs` VALUES ('509', 'admincp_accounts', '22', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 15:59:50');
INSERT INTO `admin_nqt_logs` VALUES ('510', 'admincp_account_groups', '1', 'permission', 'Update', '0|rwd,154|rwd,203|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd,3|rwd', '0|rwd,154|rwd,203|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd', 'admin', '::1', '2015-09-18 16:12:05');
INSERT INTO `admin_nqt_logs` VALUES ('511', 'admincp_accounts', '2', 'permission', 'Update', '116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '154|r--,203|rwd,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|rwd', 'admin', '::1', '2015-09-18 16:27:48');
INSERT INTO `admin_nqt_logs` VALUES ('512', 'admincp_accounts', '2', 'custom_permission', 'Update', '0', '1', 'admin', '::1', '2015-09-18 16:27:48');
INSERT INTO `admin_nqt_logs` VALUES ('513', 'companies', '84', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 16:41:20');
INSERT INTO `admin_nqt_logs` VALUES ('514', 'companies', '85', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 16:41:28');
INSERT INTO `admin_nqt_logs` VALUES ('515', 'companies', '86', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 16:41:35');
INSERT INTO `admin_nqt_logs` VALUES ('516', 'companies', '87', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 16:42:46');
INSERT INTO `admin_nqt_logs` VALUES ('517', 'companies', '87', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 16:43:16');
INSERT INTO `admin_nqt_logs` VALUES ('518', 'companies', '86', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 16:43:17');
INSERT INTO `admin_nqt_logs` VALUES ('519', 'companies', '84', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 16:43:17');
INSERT INTO `admin_nqt_logs` VALUES ('520', 'companies', '85', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-18 16:43:17');
INSERT INTO `admin_nqt_logs` VALUES ('521', 'companies', '83', 'end_date', 'Update', null, '2015-09-01 00:00:00', 'admin', '::1', '2015-09-18 16:57:50');
INSERT INTO `admin_nqt_logs` VALUES ('522', 'companies', '83', 'start_date', 'Update', null, '2015-09-19 00:00:00', 'admin', '::1', '2015-09-18 16:57:50');
INSERT INTO `admin_nqt_logs` VALUES ('523', 'companies', '88', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-18 22:30:02');
INSERT INTO `admin_nqt_logs` VALUES ('524', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:15:15');
INSERT INTO `admin_nqt_logs` VALUES ('525', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:15:16');
INSERT INTO `admin_nqt_logs` VALUES ('526', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:19:37');
INSERT INTO `admin_nqt_logs` VALUES ('527', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:19:38');
INSERT INTO `admin_nqt_logs` VALUES ('528', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:20:01');
INSERT INTO `admin_nqt_logs` VALUES ('529', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:20:02');
INSERT INTO `admin_nqt_logs` VALUES ('530', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:22:50');
INSERT INTO `admin_nqt_logs` VALUES ('531', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:22:51');
INSERT INTO `admin_nqt_logs` VALUES ('532', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:28:16');
INSERT INTO `admin_nqt_logs` VALUES ('533', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:28:17');
INSERT INTO `admin_nqt_logs` VALUES ('534', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:29:50');
INSERT INTO `admin_nqt_logs` VALUES ('535', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:29:51');
INSERT INTO `admin_nqt_logs` VALUES ('536', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:30:09');
INSERT INTO `admin_nqt_logs` VALUES ('537', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:30:10');
INSERT INTO `admin_nqt_logs` VALUES ('538', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:30:57');
INSERT INTO `admin_nqt_logs` VALUES ('539', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:30:58');
INSERT INTO `admin_nqt_logs` VALUES ('540', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:31:22');
INSERT INTO `admin_nqt_logs` VALUES ('541', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:31:23');
INSERT INTO `admin_nqt_logs` VALUES ('542', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:31:49');
INSERT INTO `admin_nqt_logs` VALUES ('543', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:31:50');
INSERT INTO `admin_nqt_logs` VALUES ('544', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:35:55');
INSERT INTO `admin_nqt_logs` VALUES ('545', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:35:57');
INSERT INTO `admin_nqt_logs` VALUES ('546', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:36:16');
INSERT INTO `admin_nqt_logs` VALUES ('547', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:36:17');
INSERT INTO `admin_nqt_logs` VALUES ('548', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:37:18');
INSERT INTO `admin_nqt_logs` VALUES ('549', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:37:19');
INSERT INTO `admin_nqt_logs` VALUES ('550', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:46:02');
INSERT INTO `admin_nqt_logs` VALUES ('551', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 12:46:03');
INSERT INTO `admin_nqt_logs` VALUES ('552', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:46:25');
INSERT INTO `admin_nqt_logs` VALUES ('553', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-19 12:46:26');
INSERT INTO `admin_nqt_logs` VALUES ('554', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 20:35:00');
INSERT INTO `admin_nqt_logs` VALUES ('555', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-19 20:35:01');
INSERT INTO `admin_nqt_logs` VALUES ('556', 'companies', '83', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-21 09:52:15');
INSERT INTO `admin_nqt_logs` VALUES ('557', 'companies', '83', 'end_date', 'Update', '2015-09-01 00:00:00', '2015-09-30 00:00:00', 'admin', '::1', '2015-09-21 09:52:16');
INSERT INTO `admin_nqt_logs` VALUES ('558', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-21 09:52:22');
INSERT INTO `admin_nqt_logs` VALUES ('559', 'companies', '83', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-21 09:53:58');
INSERT INTO `admin_nqt_logs` VALUES ('560', 'companies', '83', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-21 09:54:03');
INSERT INTO `admin_nqt_logs` VALUES ('561', 'companies', '83', 'end_date', 'Update', '2015-09-30 00:00:00', '2015-09-11 00:00:00', 'admin', '::1', '2015-09-21 09:54:23');
INSERT INTO `admin_nqt_logs` VALUES ('562', 'companies', '83', 'start_date', 'Update', '2015-09-19 00:00:00', '2015-09-09 00:00:00', 'admin', '::1', '2015-09-21 09:54:23');
INSERT INTO `admin_nqt_logs` VALUES ('563', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-21 16:29:58');
INSERT INTO `admin_nqt_logs` VALUES ('564', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-21 16:30:42');
INSERT INTO `admin_nqt_logs` VALUES ('565', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-21 16:42:01');
INSERT INTO `admin_nqt_logs` VALUES ('566', 'companies', '88', 'forever', 'update', '1', '0', 'admin', '::1', '2015-09-21 16:42:04');
INSERT INTO `admin_nqt_logs` VALUES ('567', 'companies', '88', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-21 16:42:14');
INSERT INTO `admin_nqt_logs` VALUES ('568', 'companies', '88', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-21 16:42:15');
INSERT INTO `admin_nqt_logs` VALUES ('569', 'companies', '88', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-21 16:50:23');
INSERT INTO `admin_nqt_logs` VALUES ('570', 'companies', '88', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-21 16:52:15');
INSERT INTO `admin_nqt_logs` VALUES ('571', 'companies', '90', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-21 17:07:12');
INSERT INTO `admin_nqt_logs` VALUES ('572', 'companies', '90', 'full_name', 'Update', '三谷ミタニ', 'dsfgh', 'admin', '::1', '2015-09-21 17:09:51');
INSERT INTO `admin_nqt_logs` VALUES ('573', 'companies', '90', 'full_name', 'Update', 'dsfgh', 'asdfg', 'admin', '::1', '2015-09-21 17:10:47');
INSERT INTO `admin_nqt_logs` VALUES ('574', 'companies', '90', 'logo', 'Update', '', '2015/09/e6635f0ab6d381f6a1d811f5d022662a.jpg', 'admin', '::1', '2015-09-22 08:41:01');
INSERT INTO `admin_nqt_logs` VALUES ('575', 'companies', '83', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-22 09:38:11');
INSERT INTO `admin_nqt_logs` VALUES ('576', 'companies', '90', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-22 09:38:12');
INSERT INTO `admin_nqt_logs` VALUES ('577', 'companies', '89', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-22 09:38:12');
INSERT INTO `admin_nqt_logs` VALUES ('578', 'companies', '90', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-22 09:38:44');
INSERT INTO `admin_nqt_logs` VALUES ('579', 'companies', '90', 'logo', 'Update', '2015/09/e6635f0ab6d381f6a1d811f5d022662a.jpg', '2015/09/5926254f5c2b2a7c1ba19e70a497d019.png', 'admin', '::1', '2015-09-22 10:42:02');
INSERT INTO `admin_nqt_logs` VALUES ('580', 'companies', '92', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-23 09:35:56');
INSERT INTO `admin_nqt_logs` VALUES ('581', 'companies', '92', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-23 09:35:58');
INSERT INTO `admin_nqt_logs` VALUES ('582', 'companies', '92', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-23 09:37:27');
INSERT INTO `admin_nqt_logs` VALUES ('583', 'companies', '92', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-23 09:37:28');
INSERT INTO `admin_nqt_logs` VALUES ('584', 'companies', '92', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-23 09:37:29');
INSERT INTO `admin_nqt_logs` VALUES ('585', 'companies', '93', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-23 10:05:34');
INSERT INTO `admin_nqt_logs` VALUES ('586', 'companies', '93', 'email', 'Update', 'ahl1809@yahoo.com', 'hl1809@yahoo.com', 'admin', '::1', '2015-09-23 10:18:32');
INSERT INTO `admin_nqt_logs` VALUES ('587', 'companies', '93', 'email', 'Update', 'hl1809@yahoo.com', 'ahl1809@yahoo.com', 'admin', '::1', '2015-09-23 10:18:43');
INSERT INTO `admin_nqt_logs` VALUES ('588', 'companies', '90', 'email', 'Update', 'ftest@yahoo.com', 'test@yahoo.com', 'admin', '::1', '2015-09-23 10:19:04');
INSERT INTO `admin_nqt_logs` VALUES ('589', 'companies', '93', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-23 10:19:43');
INSERT INTO `admin_nqt_logs` VALUES ('590', 'companies', '94', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-23 10:20:00');
INSERT INTO `admin_nqt_logs` VALUES ('591', 'companies', '95', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-23 10:28:33');
INSERT INTO `admin_nqt_logs` VALUES ('592', 'companies', '94', 'email', 'Update', 'ahhl1809@yahoo.com', 'hhl1809@yahoo.com', 'admin', '::1', '2015-09-23 10:30:30');
INSERT INTO `admin_nqt_logs` VALUES ('593', 'admincp_accounts', '26', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-23 10:58:22');
INSERT INTO `admin_nqt_logs` VALUES ('594', 'admincp_accounts', '26', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-23 11:00:53');
INSERT INTO `admin_nqt_logs` VALUES ('595', 'admincp_accounts', '27', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-23 11:25:18');
INSERT INTO `admin_nqt_logs` VALUES ('596', 'companies', '95', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-23 11:29:22');
INSERT INTO `admin_nqt_logs` VALUES ('597', 'companies', '95', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-23 11:29:24');
INSERT INTO `admin_nqt_logs` VALUES ('598', 'companies', '95', 'status', 'update', '1', '0', 'kimcuu1992', '::1', '2015-09-23 12:32:44');
INSERT INTO `admin_nqt_logs` VALUES ('599', 'companies', '95', 'status', 'update', '1', '0', 'kimcuu1992', '::1', '2015-09-23 12:32:45');
INSERT INTO `admin_nqt_logs` VALUES ('600', 'companies', '95', 'status', 'update', '0', '1', 'kimcuu1992', '::1', '2015-09-23 12:32:46');
INSERT INTO `admin_nqt_logs` VALUES ('601', 'companies', '95', 'status', 'update', '1', '0', 'kimcuu1992', '::1', '2015-09-23 12:32:47');
INSERT INTO `admin_nqt_logs` VALUES ('602', 'companies', '95', 'status', 'update', '0', '1', 'kimcuu1992', '::1', '2015-09-23 12:32:48');
INSERT INTO `admin_nqt_logs` VALUES ('603', 'companies', '95', 'order', 'Update', '0', '11111111111111111111111111111111111111111111111111111', 'kimcuu1992', '::1', '2015-09-23 17:35:11');
INSERT INTO `admin_nqt_logs` VALUES ('604', 'companies', '95', 'order', 'Update', '2147483647', '11111111111111111111111111111111111111111111111111111', 'kimcuu1992', '::1', '2015-09-23 17:35:35');
INSERT INTO `admin_nqt_logs` VALUES ('605', 'companies', '95', 'order', 'Update', '2147483647', '1111111111', 'kimcuu1992', '::1', '2015-09-23 17:35:52');
INSERT INTO `admin_nqt_logs` VALUES ('606', 'members', '118', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 08:15:35');
INSERT INTO `admin_nqt_logs` VALUES ('607', 'companies', '96', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 09:28:24');
INSERT INTO `admin_nqt_logs` VALUES ('608', 'companies', '96', 'full_name', 'Update', 'ac', 'acc', 'admin', '::1', '2015-09-24 09:29:19');
INSERT INTO `admin_nqt_logs` VALUES ('609', 'companies', '96', 'full_name', 'Update', 'acc', '', 'admin', '::1', '2015-09-24 09:45:30');
INSERT INTO `admin_nqt_logs` VALUES ('610', 'companies', '96', 'full_name', 'Update', '', 'a b', 'admin', '::1', '2015-09-24 09:48:36');
INSERT INTO `admin_nqt_logs` VALUES ('611', 'companies', '96', 'full_name', 'Update', 'a b', 'a v b d', 'admin', '::1', '2015-09-24 09:48:50');
INSERT INTO `admin_nqt_logs` VALUES ('612', 'companies', '96', 'full_name', 'Update', 'a v b d', 'aa b c 1 23', 'admin', '::1', '2015-09-24 09:49:48');
INSERT INTO `admin_nqt_logs` VALUES ('613', 'companies', '97', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 09:50:22');
INSERT INTO `admin_nqt_logs` VALUES ('614', 'companies', '98', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 10:00:23');
INSERT INTO `admin_nqt_logs` VALUES ('615', 'companies', '98', 'full_name', 'Update', 'kim cuu', '', 'admin', '::1', '2015-09-24 10:17:53');
INSERT INTO `admin_nqt_logs` VALUES ('616', 'companies', '98', 'full_name', 'Update', '', 'kim cuu', 'admin', '::1', '2015-09-24 10:19:19');
INSERT INTO `admin_nqt_logs` VALUES ('617', 'companies', '98', 'full_name', 'Update', 'kim cuu', '', 'admin', '::1', '2015-09-24 10:21:10');
INSERT INTO `admin_nqt_logs` VALUES ('618', 'companies', '98', 'full_name', 'Update', '', '三谷ミタニ', 'admin', '::1', '2015-09-24 10:22:11');
INSERT INTO `admin_nqt_logs` VALUES ('619', 'companies', '98', 'full_name', 'Update', '三谷ミタニ', '', 'admin', '::1', '2015-09-24 10:22:16');
INSERT INTO `admin_nqt_logs` VALUES ('620', 'companies', '98', 'full_name', 'Update', '', '三谷ミタニ', 'admin', '::1', '2015-09-24 10:31:30');
INSERT INTO `admin_nqt_logs` VALUES ('621', 'companies', '98', 'company_name', 'Update', 'test', 'ミタニ', 'admin', '::1', '2015-09-24 10:31:39');
INSERT INTO `admin_nqt_logs` VALUES ('622', 'companies', '98', 'full_name', 'Update', '三谷ミタニ', '', 'admin', '::1', '2015-09-24 10:42:44');
INSERT INTO `admin_nqt_logs` VALUES ('623', 'companies', '98', 'full_name', 'Update', '', 'kimcuu', 'admin', '::1', '2015-09-24 10:47:49');
INSERT INTO `admin_nqt_logs` VALUES ('624', 'companies', '98', 'full_name', 'Update', 'kimcuu', 'kim cuu', 'admin', '::1', '2015-09-24 10:54:30');
INSERT INTO `admin_nqt_logs` VALUES ('625', 'companies', '98', 'full_name', 'Update', 'kim cuu', '', 'admin', '::1', '2015-09-24 10:54:35');
INSERT INTO `admin_nqt_logs` VALUES ('626', 'companies', '98', 'full_name', 'Update', '', 'kim cuu', 'admin', '::1', '2015-09-24 11:05:09');
INSERT INTO `admin_nqt_logs` VALUES ('627', 'companies', '98', 'full_name', 'Update', 'kim cuu', '', 'admin', '::1', '2015-09-24 11:05:18');
INSERT INTO `admin_nqt_logs` VALUES ('628', 'companies', '98', 'full_name', 'Update', '', 'kim cuu', 'admin', '::1', '2015-09-24 11:24:19');
INSERT INTO `admin_nqt_logs` VALUES ('629', 'companies', '98', 'full_name', 'Update', 'kim cuu', 'kimcuu', 'admin', '::1', '2015-09-24 11:24:24');
INSERT INTO `admin_nqt_logs` VALUES ('630', 'companies', '98', 'full_name', 'Update', 'kimcuu', 'kim cuu', 'admin', '::1', '2015-09-24 11:27:41');
INSERT INTO `admin_nqt_logs` VALUES ('631', 'companies', '98', 'company_name', 'Update', 'ミタニ', 'test', 'admin', '::1', '2015-09-24 11:28:51');
INSERT INTO `admin_nqt_logs` VALUES ('632', 'members', '119', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 13:16:30');
INSERT INTO `admin_nqt_logs` VALUES ('633', 'members', '119', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-24 13:18:16');
INSERT INTO `admin_nqt_logs` VALUES ('634', 'members', '118', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-24 13:18:16');
INSERT INTO `admin_nqt_logs` VALUES ('635', 'members', '120', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-24 13:29:42');
INSERT INTO `admin_nqt_logs` VALUES ('636', 'admincp_accounts', '27', 'username', 'Update', 'kimcuu1992', ' kim cuu', 'admin', '::1', '2015-09-24 13:34:43');
INSERT INTO `admin_nqt_logs` VALUES ('637', 'admincp_accounts', '27', 'password', 'Update', 'e10adc3949ba59abbe56e057f20f883e', 'c4ca4238a0b923820dcc509a6f75849b', 'admin', '::1', '2015-09-24 13:34:43');
INSERT INTO `admin_nqt_logs` VALUES ('638', 'admincp_accounts', '27', 'permission', 'Update', '116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '154|rwd,203|---,164|rwd,2|rwd,1|rwd,4|rwd,159|rwd,84|---', 'admin', '::1', '2015-09-24 13:34:43');
INSERT INTO `admin_nqt_logs` VALUES ('639', 'admincp_accounts', '27', 'username', 'Update', ' kim cuu', ' kimcuu1992', 'admin', '::1', '2015-09-24 13:34:56');
INSERT INTO `admin_nqt_logs` VALUES ('640', 'admincp_accounts', '27', 'password', 'Update', 'c4ca4238a0b923820dcc509a6f75849b', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '::1', '2015-09-24 13:34:56');
INSERT INTO `admin_nqt_logs` VALUES ('641', 'admincp_accounts', '27', 'username', 'Update', ' kimcuu1992', 'kimcuu1992', 'admin', '::1', '2015-09-24 13:36:00');
INSERT INTO `admin_nqt_logs` VALUES ('642', 'members', '113', 'password', 'Update', 'bbb8aae57c104cda40c93843ad5e6db8', 'e10adc3949ba59abbe56e057f20f883e', 'kimcuu1992', '::1', '2015-09-24 13:55:25');
INSERT INTO `admin_nqt_logs` VALUES ('643', 'companies', '98', 'logo', 'Update', '', '2015/09/759298b856d4e8ea45e3706b19e40f45.jpg', 'admin', '::1', '2015-09-24 16:16:37');
INSERT INTO `admin_nqt_logs` VALUES ('644', 'companies', '97', 'katakana', 'Update', 'ミタニ', 'ミタ', 'kimcuu1992', '::1', '2015-09-24 16:21:32');
INSERT INTO `admin_nqt_logs` VALUES ('645', 'companies', '98', 'email', 'Update', 'fdtest@yahoo.com', 'fdtest@yahoo.comg', 'admin', '::1', '2015-09-24 17:44:29');
INSERT INTO `admin_nqt_logs` VALUES ('646', 'companies', '98', 'email', 'Update', 'fdtest@yahoo.comg', 'fdtest@yahoo.com', 'admin', '::1', '2015-09-24 17:44:36');
INSERT INTO `admin_nqt_logs` VALUES ('647', 'companies', '99', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-25 08:35:30');
INSERT INTO `admin_nqt_logs` VALUES ('648', 'companies', '92', 'katakana', 'Update', 'これさえあれ', 'ミタニ', 'admin', '::1', '2015-09-25 09:42:20');
INSERT INTO `admin_nqt_logs` VALUES ('649', 'companies', '92', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-25 09:42:20');
INSERT INTO `admin_nqt_logs` VALUES ('650', 'companies', '100', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-25 09:44:55');
INSERT INTO `admin_nqt_logs` VALUES ('651', 'companies', '100', 'logo', 'Update', '', '2015/09/7752bada48eba3f878884a1813abf403.jpg', 'admin', '::1', '2015-09-25 10:10:05');
INSERT INTO `admin_nqt_logs` VALUES ('652', 'companies', '100', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-25 10:27:42');
INSERT INTO `admin_nqt_logs` VALUES ('653', 'companies', '100', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-25 10:27:43');
INSERT INTO `admin_nqt_logs` VALUES ('654', 'companies', '100', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-25 10:27:45');
INSERT INTO `admin_nqt_logs` VALUES ('655', 'companies', '100', 'first', 'update', '0', '1', 'admin', '::1', '2015-09-25 10:30:07');
INSERT INTO `admin_nqt_logs` VALUES ('656', 'companies', '100', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-25 10:30:08');
INSERT INTO `admin_nqt_logs` VALUES ('657', 'members', '129', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-09-25 12:42:26');
INSERT INTO `admin_nqt_logs` VALUES ('658', 'members', '129', 'Delete', 'Delete', '', '', 'admin', '::1', '2015-09-25 12:42:47');
INSERT INTO `admin_nqt_logs` VALUES ('659', 'companies', '83', 'logo', 'Update', '2015/09/1dae25b5c54940b5c8ce351ebc98c8b4.png', '2015/09/288a3a54907b57cae67bec152c81f471.jpg', 'admin', '::1', '2015-09-25 12:53:35');
INSERT INTO `admin_nqt_logs` VALUES ('660', 'companies', '83', 'logo', 'Update', '2015/09/288a3a54907b57cae67bec152c81f471.jpg', '2015/09/25a8ab03e26d74146168c8ae5766323e.png', 'admin', '::1', '2015-09-25 13:43:04');
INSERT INTO `admin_nqt_logs` VALUES ('661', 'companies', '83', 'logo', 'Update', '2015/09/25a8ab03e26d74146168c8ae5766323e.png', '2015/09/d6a58b851dbcecdb657d1b954c301de2.jpg', 'admin', '::1', '2015-09-25 13:44:14');
INSERT INTO `admin_nqt_logs` VALUES ('662', 'companies', '83', 'katakana', 'Update', 'ミタニ', 'ミタニ ミタニ', 'admin', '::1', '2015-09-25 14:19:33');
INSERT INTO `admin_nqt_logs` VALUES ('663', 'companies', '83', 'katakana', 'Update', 'ミタニ ミタニ', 'ｶ', 'admin', '::1', '2015-09-25 14:20:07');
INSERT INTO `admin_nqt_logs` VALUES ('664', 'companies', '83', 'katakana', 'Update', 'ｶ', 'ミタニ', 'admin', '::1', '2015-09-25 14:23:56');
INSERT INTO `admin_nqt_logs` VALUES ('665', 'companies', '83', 'forever', 'update', '0', '1', 'admin', '::1', '2015-09-25 14:28:20');
INSERT INTO `admin_nqt_logs` VALUES ('666', 'companies', '83', 'first', 'update', '1', '0', 'admin', '::1', '2015-09-25 14:31:24');
INSERT INTO `admin_nqt_logs` VALUES ('667', 'companies', '83', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-25 14:56:45');
INSERT INTO `admin_nqt_logs` VALUES ('668', 'companies', '83', 'first', 'Update', '1', '0', 'admin', '::1', '2015-09-25 14:57:03');
INSERT INTO `admin_nqt_logs` VALUES ('669', 'companies', '83', 'first', 'Update', '0', '1', 'admin', '::1', '2015-09-25 15:05:11');
INSERT INTO `admin_nqt_logs` VALUES ('670', 'companies', '83', 'first', 'Update', '1', '0', 'admin', '::1', '2015-09-25 15:05:26');
INSERT INTO `admin_nqt_logs` VALUES ('671', 'companies', '83', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-25 15:15:19');
INSERT INTO `admin_nqt_logs` VALUES ('672', 'companies', '83', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-09-25 15:17:33');
INSERT INTO `admin_nqt_logs` VALUES ('673', 'companies', '83', 'katakana', 'Update', 'ミタニ', 'カ', 'admin', '::1', '2015-09-25 15:53:45');
INSERT INTO `admin_nqt_logs` VALUES ('674', 'companies', '83', 'katakana', 'Update', 'カ', 'ミタニ', 'admin', '::1', '2015-09-25 15:54:18');
INSERT INTO `admin_nqt_logs` VALUES ('675', 'companies', '83', 'full_name', 'Update', '三谷ミタニ', 'kim cuu', 'admin', '::1', '2015-09-30 08:40:46');
INSERT INTO `admin_nqt_logs` VALUES ('676', 'companies', '83', 'full_name', 'Update', 'kim cuu', 'aaa a', 'admin', '::1', '2015-09-30 08:41:25');
INSERT INTO `admin_nqt_logs` VALUES ('677', 'companies', '83', 'full_name', 'Update', 'aaa a', '', 'admin', '::1', '2015-09-30 08:43:08');
INSERT INTO `admin_nqt_logs` VALUES ('678', 'companies', '83', 'full_name', 'Update', '', 'aaaaaaaccccssaaaaa', 'admin', '::1', '2015-09-30 08:44:04');
INSERT INTO `admin_nqt_logs` VALUES ('679', 'companies', '83', 'full_name', 'Update', 'aaaaaaaccccssaaaaa', 'aaaaaaa ccccssaaaaa', 'admin', '::1', '2015-09-30 08:44:24');
INSERT INTO `admin_nqt_logs` VALUES ('680', 'companies', '83', 'full_name', 'Update', 'aaaaaaa ccccssaaaaa', 'kim cuu', 'admin', '::1', '2015-09-30 08:44:38');
INSERT INTO `admin_nqt_logs` VALUES ('681', 'companies', '83', 'full_name', 'Update', 'kim cuu', 'kimcuu', 'admin', '::1', '2015-09-30 08:50:45');
INSERT INTO `admin_nqt_logs` VALUES ('682', 'companies', '83', 'full_name', 'Update', 'kimcuu', 'kim cuu', 'admin', '::1', '2015-09-30 08:50:54');
INSERT INTO `admin_nqt_logs` VALUES ('683', 'companies', '83', 'full_name', 'Update', 'kim cuu', 'kim cuucuu', 'admin', '::1', '2015-09-30 08:53:12');
INSERT INTO `admin_nqt_logs` VALUES ('684', 'members', '133', 'full_name', 'Update', 'a', 'afffddđfff', 'admin', '::1', '2015-09-30 08:54:53');
INSERT INTO `admin_nqt_logs` VALUES ('685', 'members', '133', 'company_name', 'Update', 'b', 'bdđ', 'admin', '::1', '2015-09-30 08:54:53');
INSERT INTO `admin_nqt_logs` VALUES ('686', 'companies', '83', 'full_name', 'Update', 'kim cuucuu', 'kim   cuucuu', 'admin', '::1', '2015-09-30 08:58:14');
INSERT INTO `admin_nqt_logs` VALUES ('687', 'companies', '83', 'full_name', 'Update', 'kim   cuucuu', '', 'admin', '::1', '2015-09-30 08:58:24');
INSERT INTO `admin_nqt_logs` VALUES ('688', 'companies', '83', 'full_name', 'Update', '', 'kim cuu', 'admin', '::1', '2015-09-30 09:09:44');
INSERT INTO `admin_nqt_logs` VALUES ('689', 'companies', '83', 'full_name', 'Update', 'kim cuu', 'kim cuu<', 'admin', '::1', '2015-09-30 09:11:01');
INSERT INTO `admin_nqt_logs` VALUES ('690', 'companies', '83', 'full_name', 'Update', 'kim cuu<', 'kim cuu', 'admin', '::1', '2015-09-30 09:14:22');
INSERT INTO `admin_nqt_logs` VALUES ('691', 'companies', '83', 'service_name', 'Update', 'test', 'kim cuu', 'admin', '::1', '2015-09-30 09:15:36');
INSERT INTO `admin_nqt_logs` VALUES ('692', 'companies', '83', 'full_name', 'Update', 'kim cuu', 'a b', 'admin', '::1', '2015-09-30 09:22:20');
INSERT INTO `admin_nqt_logs` VALUES ('693', 'members', '133', 'company_name', 'Update', 'bdđ', 'ffbffffdđ', 'admin', '::1', '2015-09-30 09:29:27');
INSERT INTO `admin_nqt_logs` VALUES ('694', 'members', '113', 'full_name', 'Update', 'kimcuu', '丈叱晴隆', 'admin', '::1', '2015-09-30 09:38:45');
INSERT INTO `admin_nqt_logs` VALUES ('695', 'members', '113', 'company_name', 'Update', 'jv-it', '丈叱晴隆', 'admin', '::1', '2015-09-30 09:38:45');
INSERT INTO `admin_nqt_logs` VALUES ('696', 'members', '113', 'full_name', 'Update', '丈叱晴隆', 'kimcuu', 'admin', '::1', '2015-09-30 09:39:18');
INSERT INTO `admin_nqt_logs` VALUES ('697', 'members', '113', 'company_name', 'Update', '丈叱晴隆', 'jv-it', 'admin', '::1', '2015-09-30 09:39:18');
INSERT INTO `admin_nqt_logs` VALUES ('698', 'companies', '83', 'full_name', 'Update', 'a b', '丈叱晴隆', 'admin', '::1', '2015-09-30 09:39:43');
INSERT INTO `admin_nqt_logs` VALUES ('699', 'companies', '83', 'company_name', 'Update', 'test', '丈叱晴隆', 'admin', '::1', '2015-09-30 09:39:43');
INSERT INTO `admin_nqt_logs` VALUES ('700', 'companies', '83', 'service_name', 'Update', 'kim cuu', '丈叱晴隆', 'admin', '::1', '2015-09-30 09:39:43');
INSERT INTO `admin_nqt_logs` VALUES ('701', 'members', '133', 'full_name', 'Update', 'afffddđfff', '丈叱晴隆', 'admin', '::1', '2015-09-30 10:20:43');
INSERT INTO `admin_nqt_logs` VALUES ('702', 'members', '133', 'company_name', 'Update', 'ffbffffdđ', '丈叱晴隆', 'admin', '::1', '2015-09-30 10:20:43');
INSERT INTO `admin_nqt_logs` VALUES ('703', 'members', '133', 'password', 'Update', 'e10adc3949ba59abbe56e057f20f883e', 'f4eb4c1975b70028492dba13530d8710', 'admin', '::1', '2015-09-30 10:21:19');
INSERT INTO `admin_nqt_logs` VALUES ('704', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-30 11:24:37');
INSERT INTO `admin_nqt_logs` VALUES ('705', 'companies', '83', 'forever', 'Update', '1', '0', 'admin', '::1', '2015-09-30 11:24:54');
INSERT INTO `admin_nqt_logs` VALUES ('706', 'companies', '83', 'end_date', 'Update', '2015-09-11 00:00:00', '2015-09-30 00:00:00', 'admin', '::1', '2015-09-30 11:24:54');
INSERT INTO `admin_nqt_logs` VALUES ('707', 'companies', '83', 'start_date', 'Update', '2015-09-09 00:00:00', '2015-09-30 00:00:00', 'admin', '::1', '2015-09-30 11:24:54');
INSERT INTO `admin_nqt_logs` VALUES ('708', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-09-30 11:28:07');
INSERT INTO `admin_nqt_logs` VALUES ('709', 'companies', '83', 'end_date', 'Update', '2015-09-30 00:00:00', '2015-09-29 00:00:00', 'admin', '::1', '2015-09-30 12:09:44');
INSERT INTO `admin_nqt_logs` VALUES ('710', 'companies', '83', 'start_date', 'Update', '2015-09-30 00:00:00', '2015-09-29 00:00:00', 'admin', '::1', '2015-09-30 12:09:44');
INSERT INTO `admin_nqt_logs` VALUES ('711', 'companies', '83', 'end_date', 'Update', '2015-09-29 00:00:00', '2015-10-01 00:00:00', 'admin', '::1', '2015-09-30 12:09:55');
INSERT INTO `admin_nqt_logs` VALUES ('712', 'companies', '83', 'start_date', 'Update', '2015-09-29 00:00:00', '2015-09-30 00:00:00', 'admin', '::1', '2015-09-30 12:09:55');
INSERT INTO `admin_nqt_logs` VALUES ('713', 'companies', '83', 'end_date', 'Update', '2015-10-01 00:00:00', '2015-10-30 00:00:00', 'admin', '::1', '2015-09-30 12:10:04');
INSERT INTO `admin_nqt_logs` VALUES ('714', 'companies', '83', 'start_date', 'Update', '2015-09-30 00:00:00', '2015-09-16 00:00:00', 'admin', '::1', '2015-09-30 12:10:55');
INSERT INTO `admin_nqt_logs` VALUES ('715', 'companies', '83', 'start_date', 'Update', '2015-09-16 00:00:00', '2015-09-30 00:00:00', 'admin', '::1', '2015-09-30 12:11:05');
INSERT INTO `admin_nqt_logs` VALUES ('716', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-09-30 12:21:51');
INSERT INTO `admin_nqt_logs` VALUES ('717', 'members', '133', 'password', 'Update', 'f4eb4c1975b70028492dba13530d8710', 'c4ca4238a0b923820dcc509a6f75849b', 'admin', '::1', '2015-09-30 16:13:09');
INSERT INTO `admin_nqt_logs` VALUES ('718', 'members', '133', 'full_name', 'Update', '丈叱晴隆', 'a', 'admin', '::1', '2015-09-30 16:14:21');
INSERT INTO `admin_nqt_logs` VALUES ('719', 'members', '133', 'company_name', 'Update', '丈叱晴隆', 'a', 'admin', '::1', '2015-09-30 16:14:21');
INSERT INTO `admin_nqt_logs` VALUES ('720', 'members', '133', 'password', 'Update', 'c4ca4238a0b923820dcc509a6f75849b', '0cc175b9c0f1b6a831c399e269772661', 'admin', '::1', '2015-09-30 16:14:21');
INSERT INTO `admin_nqt_logs` VALUES ('721', 'members', '133', 'password', 'Update', '0cc175b9c0f1b6a831c399e269772661', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '::1', '2015-10-01 10:35:29');
INSERT INTO `admin_nqt_logs` VALUES ('722', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 15:36:40');
INSERT INTO `admin_nqt_logs` VALUES ('723', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 15:37:54');
INSERT INTO `admin_nqt_logs` VALUES ('724', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 15:38:10');
INSERT INTO `admin_nqt_logs` VALUES ('725', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 15:39:00');
INSERT INTO `admin_nqt_logs` VALUES ('726', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 15:39:33');
INSERT INTO `admin_nqt_logs` VALUES ('727', 'companies', '101', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-05 15:46:53');
INSERT INTO `admin_nqt_logs` VALUES ('728', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 15:47:02');
INSERT INTO `admin_nqt_logs` VALUES ('729', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 15:47:02');
INSERT INTO `admin_nqt_logs` VALUES ('730', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 15:47:27');
INSERT INTO `admin_nqt_logs` VALUES ('731', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 15:47:27');
INSERT INTO `admin_nqt_logs` VALUES ('732', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:02:38');
INSERT INTO `admin_nqt_logs` VALUES ('733', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:02:53');
INSERT INTO `admin_nqt_logs` VALUES ('734', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:03:11');
INSERT INTO `admin_nqt_logs` VALUES ('735', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:07:47');
INSERT INTO `admin_nqt_logs` VALUES ('736', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:08:08');
INSERT INTO `admin_nqt_logs` VALUES ('737', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:09:55');
INSERT INTO `admin_nqt_logs` VALUES ('738', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:09:55');
INSERT INTO `admin_nqt_logs` VALUES ('739', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:10:24');
INSERT INTO `admin_nqt_logs` VALUES ('740', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:11:40');
INSERT INTO `admin_nqt_logs` VALUES ('741', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:11:59');
INSERT INTO `admin_nqt_logs` VALUES ('742', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:13:16');
INSERT INTO `admin_nqt_logs` VALUES ('743', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:13:33');
INSERT INTO `admin_nqt_logs` VALUES ('744', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:14:16');
INSERT INTO `admin_nqt_logs` VALUES ('745', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:14:35');
INSERT INTO `admin_nqt_logs` VALUES ('746', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:16:02');
INSERT INTO `admin_nqt_logs` VALUES ('747', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:16:23');
INSERT INTO `admin_nqt_logs` VALUES ('748', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:16:24');
INSERT INTO `admin_nqt_logs` VALUES ('749', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:16:40');
INSERT INTO `admin_nqt_logs` VALUES ('750', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:16:41');
INSERT INTO `admin_nqt_logs` VALUES ('751', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:16:57');
INSERT INTO `admin_nqt_logs` VALUES ('752', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:16:58');
INSERT INTO `admin_nqt_logs` VALUES ('753', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:17:44');
INSERT INTO `admin_nqt_logs` VALUES ('754', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:17:45');
INSERT INTO `admin_nqt_logs` VALUES ('755', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:18:00');
INSERT INTO `admin_nqt_logs` VALUES ('756', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:18:01');
INSERT INTO `admin_nqt_logs` VALUES ('757', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:18:04');
INSERT INTO `admin_nqt_logs` VALUES ('758', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:18:05');
INSERT INTO `admin_nqt_logs` VALUES ('759', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:18:32');
INSERT INTO `admin_nqt_logs` VALUES ('760', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:18:33');
INSERT INTO `admin_nqt_logs` VALUES ('761', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:21:28');
INSERT INTO `admin_nqt_logs` VALUES ('762', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:21:29');
INSERT INTO `admin_nqt_logs` VALUES ('763', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:21:47');
INSERT INTO `admin_nqt_logs` VALUES ('764', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:21:47');
INSERT INTO `admin_nqt_logs` VALUES ('765', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:22:22');
INSERT INTO `admin_nqt_logs` VALUES ('766', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:22:22');
INSERT INTO `admin_nqt_logs` VALUES ('767', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:23:00');
INSERT INTO `admin_nqt_logs` VALUES ('768', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:23:02');
INSERT INTO `admin_nqt_logs` VALUES ('769', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:23:18');
INSERT INTO `admin_nqt_logs` VALUES ('770', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:23:55');
INSERT INTO `admin_nqt_logs` VALUES ('771', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:24:05');
INSERT INTO `admin_nqt_logs` VALUES ('772', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:25:07');
INSERT INTO `admin_nqt_logs` VALUES ('773', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:25:21');
INSERT INTO `admin_nqt_logs` VALUES ('774', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:27:09');
INSERT INTO `admin_nqt_logs` VALUES ('775', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:27:26');
INSERT INTO `admin_nqt_logs` VALUES ('776', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:32:18');
INSERT INTO `admin_nqt_logs` VALUES ('777', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:32:21');
INSERT INTO `admin_nqt_logs` VALUES ('778', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:32:45');
INSERT INTO `admin_nqt_logs` VALUES ('779', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:32:46');
INSERT INTO `admin_nqt_logs` VALUES ('780', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:32:59');
INSERT INTO `admin_nqt_logs` VALUES ('781', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:33:01');
INSERT INTO `admin_nqt_logs` VALUES ('782', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:33:01');
INSERT INTO `admin_nqt_logs` VALUES ('783', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:33:23');
INSERT INTO `admin_nqt_logs` VALUES ('784', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:33:24');
INSERT INTO `admin_nqt_logs` VALUES ('785', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:34:14');
INSERT INTO `admin_nqt_logs` VALUES ('786', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:34:15');
INSERT INTO `admin_nqt_logs` VALUES ('787', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:34:53');
INSERT INTO `admin_nqt_logs` VALUES ('788', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:34:54');
INSERT INTO `admin_nqt_logs` VALUES ('789', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:35:25');
INSERT INTO `admin_nqt_logs` VALUES ('790', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:41:21');
INSERT INTO `admin_nqt_logs` VALUES ('791', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:42:10');
INSERT INTO `admin_nqt_logs` VALUES ('792', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:42:11');
INSERT INTO `admin_nqt_logs` VALUES ('793', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:42:30');
INSERT INTO `admin_nqt_logs` VALUES ('794', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:42:31');
INSERT INTO `admin_nqt_logs` VALUES ('795', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:42:49');
INSERT INTO `admin_nqt_logs` VALUES ('796', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:42:50');
INSERT INTO `admin_nqt_logs` VALUES ('797', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:48:40');
INSERT INTO `admin_nqt_logs` VALUES ('798', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:49:30');
INSERT INTO `admin_nqt_logs` VALUES ('799', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:49:56');
INSERT INTO `admin_nqt_logs` VALUES ('800', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:50:35');
INSERT INTO `admin_nqt_logs` VALUES ('801', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:50:58');
INSERT INTO `admin_nqt_logs` VALUES ('802', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:52:48');
INSERT INTO `admin_nqt_logs` VALUES ('803', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:53:05');
INSERT INTO `admin_nqt_logs` VALUES ('804', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:53:05');
INSERT INTO `admin_nqt_logs` VALUES ('805', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:53:07');
INSERT INTO `admin_nqt_logs` VALUES ('806', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:53:08');
INSERT INTO `admin_nqt_logs` VALUES ('807', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:53:08');
INSERT INTO `admin_nqt_logs` VALUES ('808', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:56:12');
INSERT INTO `admin_nqt_logs` VALUES ('809', 'companies', '83', 'end_date', 'Update', '2015-10-30 00:00:00', '2015-10-01 00:00:00', 'admin', '::1', '2015-10-05 16:56:54');
INSERT INTO `admin_nqt_logs` VALUES ('810', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:57:07');
INSERT INTO `admin_nqt_logs` VALUES ('811', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:57:08');
INSERT INTO `admin_nqt_logs` VALUES ('812', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:57:33');
INSERT INTO `admin_nqt_logs` VALUES ('813', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:57:34');
INSERT INTO `admin_nqt_logs` VALUES ('814', 'companies', '83', 'end_date', 'Update', '2015-10-01 00:00:00', '2015-10-31 00:00:00', 'admin', '::1', '2015-10-05 16:58:52');
INSERT INTO `admin_nqt_logs` VALUES ('815', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 16:59:17');
INSERT INTO `admin_nqt_logs` VALUES ('816', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 16:59:53');
INSERT INTO `admin_nqt_logs` VALUES ('817', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:00:18');
INSERT INTO `admin_nqt_logs` VALUES ('818', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 17:01:38');
INSERT INTO `admin_nqt_logs` VALUES ('819', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:02:03');
INSERT INTO `admin_nqt_logs` VALUES ('820', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 17:03:50');
INSERT INTO `admin_nqt_logs` VALUES ('821', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:04:06');
INSERT INTO `admin_nqt_logs` VALUES ('822', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 17:05:36');
INSERT INTO `admin_nqt_logs` VALUES ('823', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:06:00');
INSERT INTO `admin_nqt_logs` VALUES ('824', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 17:17:45');
INSERT INTO `admin_nqt_logs` VALUES ('825', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:18:05');
INSERT INTO `admin_nqt_logs` VALUES ('826', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-05 17:18:35');
INSERT INTO `admin_nqt_logs` VALUES ('827', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-05 17:18:51');
INSERT INTO `admin_nqt_logs` VALUES ('828', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:11:40');
INSERT INTO `admin_nqt_logs` VALUES ('829', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:11:55');
INSERT INTO `admin_nqt_logs` VALUES ('830', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:11:56');
INSERT INTO `admin_nqt_logs` VALUES ('831', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:18:31');
INSERT INTO `admin_nqt_logs` VALUES ('832', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:18:37');
INSERT INTO `admin_nqt_logs` VALUES ('833', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:18:57');
INSERT INTO `admin_nqt_logs` VALUES ('834', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:20:27');
INSERT INTO `admin_nqt_logs` VALUES ('835', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:20:48');
INSERT INTO `admin_nqt_logs` VALUES ('836', 'companies', '102', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-06 08:21:20');
INSERT INTO `admin_nqt_logs` VALUES ('837', 'companies', '103', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-06 08:21:36');
INSERT INTO `admin_nqt_logs` VALUES ('838', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:21:43');
INSERT INTO `admin_nqt_logs` VALUES ('839', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:02');
INSERT INTO `admin_nqt_logs` VALUES ('840', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:03');
INSERT INTO `admin_nqt_logs` VALUES ('841', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:05');
INSERT INTO `admin_nqt_logs` VALUES ('842', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:22:23');
INSERT INTO `admin_nqt_logs` VALUES ('843', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:22:23');
INSERT INTO `admin_nqt_logs` VALUES ('844', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:22:24');
INSERT INTO `admin_nqt_logs` VALUES ('845', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:46');
INSERT INTO `admin_nqt_logs` VALUES ('846', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:47');
INSERT INTO `admin_nqt_logs` VALUES ('847', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:22:48');
INSERT INTO `admin_nqt_logs` VALUES ('848', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:24:55');
INSERT INTO `admin_nqt_logs` VALUES ('849', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:24:55');
INSERT INTO `admin_nqt_logs` VALUES ('850', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:24:56');
INSERT INTO `admin_nqt_logs` VALUES ('851', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:25:20');
INSERT INTO `admin_nqt_logs` VALUES ('852', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:25:21');
INSERT INTO `admin_nqt_logs` VALUES ('853', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:25:22');
INSERT INTO `admin_nqt_logs` VALUES ('854', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:25:27');
INSERT INTO `admin_nqt_logs` VALUES ('855', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:25:28');
INSERT INTO `admin_nqt_logs` VALUES ('856', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:25:29');
INSERT INTO `admin_nqt_logs` VALUES ('857', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:27:18');
INSERT INTO `admin_nqt_logs` VALUES ('858', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:27:19');
INSERT INTO `admin_nqt_logs` VALUES ('859', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:27:19');
INSERT INTO `admin_nqt_logs` VALUES ('860', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:27:59');
INSERT INTO `admin_nqt_logs` VALUES ('861', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:27:59');
INSERT INTO `admin_nqt_logs` VALUES ('862', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:28:00');
INSERT INTO `admin_nqt_logs` VALUES ('863', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:30:13');
INSERT INTO `admin_nqt_logs` VALUES ('864', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:30:14');
INSERT INTO `admin_nqt_logs` VALUES ('865', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:33:56');
INSERT INTO `admin_nqt_logs` VALUES ('866', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:33:56');
INSERT INTO `admin_nqt_logs` VALUES ('867', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:34:18');
INSERT INTO `admin_nqt_logs` VALUES ('868', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:34:19');
INSERT INTO `admin_nqt_logs` VALUES ('869', 'companies', '103', 'logo', 'Update', '', '2015/10/5a6069ad554ee24d47d7329c51e6c22c.jpg', 'admin', '::1', '2015-10-06 08:34:43');
INSERT INTO `admin_nqt_logs` VALUES ('870', 'companies', '102', 'logo', 'Update', '', '2015/10/09a49a726187c71f1f809c3654ed046c.jpg', 'admin', '::1', '2015-10-06 08:34:55');
INSERT INTO `admin_nqt_logs` VALUES ('871', 'companies', '101', 'logo', 'Update', '', '2015/10/273dddd12b609244102f6d4c92f23caf.jpg', 'admin', '::1', '2015-10-06 08:35:06');
INSERT INTO `admin_nqt_logs` VALUES ('872', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:35:12');
INSERT INTO `admin_nqt_logs` VALUES ('873', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:35:12');
INSERT INTO `admin_nqt_logs` VALUES ('874', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:35:37');
INSERT INTO `admin_nqt_logs` VALUES ('875', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:35:37');
INSERT INTO `admin_nqt_logs` VALUES ('876', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:35:48');
INSERT INTO `admin_nqt_logs` VALUES ('877', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:37:19');
INSERT INTO `admin_nqt_logs` VALUES ('878', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:39:13');
INSERT INTO `admin_nqt_logs` VALUES ('879', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:40:57');
INSERT INTO `admin_nqt_logs` VALUES ('880', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:41:45');
INSERT INTO `admin_nqt_logs` VALUES ('881', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:41:52');
INSERT INTO `admin_nqt_logs` VALUES ('882', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:46:24');
INSERT INTO `admin_nqt_logs` VALUES ('883', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:48:42');
INSERT INTO `admin_nqt_logs` VALUES ('884', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:48:50');
INSERT INTO `admin_nqt_logs` VALUES ('885', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:48:50');
INSERT INTO `admin_nqt_logs` VALUES ('886', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:52:43');
INSERT INTO `admin_nqt_logs` VALUES ('887', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:52:43');
INSERT INTO `admin_nqt_logs` VALUES ('888', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:52:54');
INSERT INTO `admin_nqt_logs` VALUES ('889', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:52:55');
INSERT INTO `admin_nqt_logs` VALUES ('890', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:52:55');
INSERT INTO `admin_nqt_logs` VALUES ('891', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 08:52:56');
INSERT INTO `admin_nqt_logs` VALUES ('892', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:56:19');
INSERT INTO `admin_nqt_logs` VALUES ('893', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 08:57:51');
INSERT INTO `admin_nqt_logs` VALUES ('894', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:01:04');
INSERT INTO `admin_nqt_logs` VALUES ('895', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:04:40');
INSERT INTO `admin_nqt_logs` VALUES ('896', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:25:39');
INSERT INTO `admin_nqt_logs` VALUES ('897', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:25:39');
INSERT INTO `admin_nqt_logs` VALUES ('898', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:26:09');
INSERT INTO `admin_nqt_logs` VALUES ('899', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:26:10');
INSERT INTO `admin_nqt_logs` VALUES ('900', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:26:51');
INSERT INTO `admin_nqt_logs` VALUES ('901', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:26:52');
INSERT INTO `admin_nqt_logs` VALUES ('902', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:28:37');
INSERT INTO `admin_nqt_logs` VALUES ('903', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:28:37');
INSERT INTO `admin_nqt_logs` VALUES ('904', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:32:40');
INSERT INTO `admin_nqt_logs` VALUES ('905', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:32:40');
INSERT INTO `admin_nqt_logs` VALUES ('906', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:36:29');
INSERT INTO `admin_nqt_logs` VALUES ('907', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:36:32');
INSERT INTO `admin_nqt_logs` VALUES ('908', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:49:33');
INSERT INTO `admin_nqt_logs` VALUES ('909', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:49:34');
INSERT INTO `admin_nqt_logs` VALUES ('910', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:49:55');
INSERT INTO `admin_nqt_logs` VALUES ('911', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:49:56');
INSERT INTO `admin_nqt_logs` VALUES ('912', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:55:57');
INSERT INTO `admin_nqt_logs` VALUES ('913', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:55:57');
INSERT INTO `admin_nqt_logs` VALUES ('914', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:56:05');
INSERT INTO `admin_nqt_logs` VALUES ('915', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:56:05');
INSERT INTO `admin_nqt_logs` VALUES ('916', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:58:05');
INSERT INTO `admin_nqt_logs` VALUES ('917', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:58:05');
INSERT INTO `admin_nqt_logs` VALUES ('918', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:58:45');
INSERT INTO `admin_nqt_logs` VALUES ('919', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 09:58:45');
INSERT INTO `admin_nqt_logs` VALUES ('920', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:59:13');
INSERT INTO `admin_nqt_logs` VALUES ('921', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 09:59:13');
INSERT INTO `admin_nqt_logs` VALUES ('922', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:02:41');
INSERT INTO `admin_nqt_logs` VALUES ('923', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:02:41');
INSERT INTO `admin_nqt_logs` VALUES ('924', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:02:59');
INSERT INTO `admin_nqt_logs` VALUES ('925', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:02:59');
INSERT INTO `admin_nqt_logs` VALUES ('926', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:21:11');
INSERT INTO `admin_nqt_logs` VALUES ('927', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:22:31');
INSERT INTO `admin_nqt_logs` VALUES ('928', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:22:55');
INSERT INTO `admin_nqt_logs` VALUES ('929', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:22:55');
INSERT INTO `admin_nqt_logs` VALUES ('930', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:22:56');
INSERT INTO `admin_nqt_logs` VALUES ('931', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:22:57');
INSERT INTO `admin_nqt_logs` VALUES ('932', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:23:08');
INSERT INTO `admin_nqt_logs` VALUES ('933', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:23:08');
INSERT INTO `admin_nqt_logs` VALUES ('934', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:23:09');
INSERT INTO `admin_nqt_logs` VALUES ('935', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:23:09');
INSERT INTO `admin_nqt_logs` VALUES ('936', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:23:19');
INSERT INTO `admin_nqt_logs` VALUES ('937', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:23:19');
INSERT INTO `admin_nqt_logs` VALUES ('938', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:25:56');
INSERT INTO `admin_nqt_logs` VALUES ('939', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:27:52');
INSERT INTO `admin_nqt_logs` VALUES ('940', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:29:21');
INSERT INTO `admin_nqt_logs` VALUES ('941', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:29:21');
INSERT INTO `admin_nqt_logs` VALUES ('942', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:29:36');
INSERT INTO `admin_nqt_logs` VALUES ('943', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:29:37');
INSERT INTO `admin_nqt_logs` VALUES ('944', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:31:09');
INSERT INTO `admin_nqt_logs` VALUES ('945', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:32:15');
INSERT INTO `admin_nqt_logs` VALUES ('946', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:32:15');
INSERT INTO `admin_nqt_logs` VALUES ('947', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:32:16');
INSERT INTO `admin_nqt_logs` VALUES ('948', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:32:32');
INSERT INTO `admin_nqt_logs` VALUES ('949', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:32:33');
INSERT INTO `admin_nqt_logs` VALUES ('950', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:33:03');
INSERT INTO `admin_nqt_logs` VALUES ('951', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:34:12');
INSERT INTO `admin_nqt_logs` VALUES ('952', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:34:31');
INSERT INTO `admin_nqt_logs` VALUES ('953', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:37:27');
INSERT INTO `admin_nqt_logs` VALUES ('954', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:37:27');
INSERT INTO `admin_nqt_logs` VALUES ('955', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:37:28');
INSERT INTO `admin_nqt_logs` VALUES ('956', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:37:56');
INSERT INTO `admin_nqt_logs` VALUES ('957', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:37:56');
INSERT INTO `admin_nqt_logs` VALUES ('958', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:46:43');
INSERT INTO `admin_nqt_logs` VALUES ('959', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:47:23');
INSERT INTO `admin_nqt_logs` VALUES ('960', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:47:24');
INSERT INTO `admin_nqt_logs` VALUES ('961', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:47:25');
INSERT INTO `admin_nqt_logs` VALUES ('962', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:47:38');
INSERT INTO `admin_nqt_logs` VALUES ('963', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:47:41');
INSERT INTO `admin_nqt_logs` VALUES ('964', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:51:35');
INSERT INTO `admin_nqt_logs` VALUES ('965', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:51:35');
INSERT INTO `admin_nqt_logs` VALUES ('966', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:51:50');
INSERT INTO `admin_nqt_logs` VALUES ('967', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:51:51');
INSERT INTO `admin_nqt_logs` VALUES ('968', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:52:21');
INSERT INTO `admin_nqt_logs` VALUES ('969', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:52:22');
INSERT INTO `admin_nqt_logs` VALUES ('970', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:38');
INSERT INTO `admin_nqt_logs` VALUES ('971', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:39');
INSERT INTO `admin_nqt_logs` VALUES ('972', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:45');
INSERT INTO `admin_nqt_logs` VALUES ('973', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:52:47');
INSERT INTO `admin_nqt_logs` VALUES ('974', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:52:48');
INSERT INTO `admin_nqt_logs` VALUES ('975', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:52:48');
INSERT INTO `admin_nqt_logs` VALUES ('976', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:54');
INSERT INTO `admin_nqt_logs` VALUES ('977', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:54');
INSERT INTO `admin_nqt_logs` VALUES ('978', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:52:59');
INSERT INTO `admin_nqt_logs` VALUES ('979', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:53:03');
INSERT INTO `admin_nqt_logs` VALUES ('980', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:53:03');
INSERT INTO `admin_nqt_logs` VALUES ('981', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:53:26');
INSERT INTO `admin_nqt_logs` VALUES ('982', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:53:45');
INSERT INTO `admin_nqt_logs` VALUES ('983', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:55:07');
INSERT INTO `admin_nqt_logs` VALUES ('984', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:55:09');
INSERT INTO `admin_nqt_logs` VALUES ('985', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:56:50');
INSERT INTO `admin_nqt_logs` VALUES ('986', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:56:51');
INSERT INTO `admin_nqt_logs` VALUES ('987', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 10:56:51');
INSERT INTO `admin_nqt_logs` VALUES ('988', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:58:44');
INSERT INTO `admin_nqt_logs` VALUES ('989', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 10:58:44');
INSERT INTO `admin_nqt_logs` VALUES ('990', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:00:01');
INSERT INTO `admin_nqt_logs` VALUES ('991', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:00:01');
INSERT INTO `admin_nqt_logs` VALUES ('992', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:00:18');
INSERT INTO `admin_nqt_logs` VALUES ('993', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:09:48');
INSERT INTO `admin_nqt_logs` VALUES ('994', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:09:56');
INSERT INTO `admin_nqt_logs` VALUES ('995', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:10:45');
INSERT INTO `admin_nqt_logs` VALUES ('996', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:11:10');
INSERT INTO `admin_nqt_logs` VALUES ('997', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:11:47');
INSERT INTO `admin_nqt_logs` VALUES ('998', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:11:57');
INSERT INTO `admin_nqt_logs` VALUES ('999', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:12:32');
INSERT INTO `admin_nqt_logs` VALUES ('1000', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:12:42');
INSERT INTO `admin_nqt_logs` VALUES ('1001', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:12:43');
INSERT INTO `admin_nqt_logs` VALUES ('1002', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:14:36');
INSERT INTO `admin_nqt_logs` VALUES ('1003', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:14:46');
INSERT INTO `admin_nqt_logs` VALUES ('1004', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:15:06');
INSERT INTO `admin_nqt_logs` VALUES ('1005', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:15:54');
INSERT INTO `admin_nqt_logs` VALUES ('1006', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:15:55');
INSERT INTO `admin_nqt_logs` VALUES ('1007', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:16:26');
INSERT INTO `admin_nqt_logs` VALUES ('1008', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:19:05');
INSERT INTO `admin_nqt_logs` VALUES ('1009', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:19:06');
INSERT INTO `admin_nqt_logs` VALUES ('1010', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:19:14');
INSERT INTO `admin_nqt_logs` VALUES ('1011', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:19:14');
INSERT INTO `admin_nqt_logs` VALUES ('1012', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:19:15');
INSERT INTO `admin_nqt_logs` VALUES ('1013', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 11:19:15');
INSERT INTO `admin_nqt_logs` VALUES ('1014', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:19:34');
INSERT INTO `admin_nqt_logs` VALUES ('1015', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:20:12');
INSERT INTO `admin_nqt_logs` VALUES ('1016', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 11:20:32');
INSERT INTO `admin_nqt_logs` VALUES ('1017', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:31:19');
INSERT INTO `admin_nqt_logs` VALUES ('1018', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:31:20');
INSERT INTO `admin_nqt_logs` VALUES ('1019', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:31:21');
INSERT INTO `admin_nqt_logs` VALUES ('1020', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:31:38');
INSERT INTO `admin_nqt_logs` VALUES ('1021', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:31:55');
INSERT INTO `admin_nqt_logs` VALUES ('1022', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:32:02');
INSERT INTO `admin_nqt_logs` VALUES ('1023', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:33:37');
INSERT INTO `admin_nqt_logs` VALUES ('1024', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:33:38');
INSERT INTO `admin_nqt_logs` VALUES ('1025', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:34:11');
INSERT INTO `admin_nqt_logs` VALUES ('1026', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:21');
INSERT INTO `admin_nqt_logs` VALUES ('1027', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:22');
INSERT INTO `admin_nqt_logs` VALUES ('1028', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:22');
INSERT INTO `admin_nqt_logs` VALUES ('1029', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:23');
INSERT INTO `admin_nqt_logs` VALUES ('1030', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:35:48');
INSERT INTO `admin_nqt_logs` VALUES ('1031', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:35:48');
INSERT INTO `admin_nqt_logs` VALUES ('1032', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:35:49');
INSERT INTO `admin_nqt_logs` VALUES ('1033', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:35:49');
INSERT INTO `admin_nqt_logs` VALUES ('1034', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:58');
INSERT INTO `admin_nqt_logs` VALUES ('1035', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:58');
INSERT INTO `admin_nqt_logs` VALUES ('1036', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:59');
INSERT INTO `admin_nqt_logs` VALUES ('1037', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:35:59');
INSERT INTO `admin_nqt_logs` VALUES ('1038', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:37:19');
INSERT INTO `admin_nqt_logs` VALUES ('1039', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:37:20');
INSERT INTO `admin_nqt_logs` VALUES ('1040', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:37:20');
INSERT INTO `admin_nqt_logs` VALUES ('1041', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:37:21');
INSERT INTO `admin_nqt_logs` VALUES ('1042', 'companies', '103', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:38:28');
INSERT INTO `admin_nqt_logs` VALUES ('1043', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:38:28');
INSERT INTO `admin_nqt_logs` VALUES ('1044', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:38:29');
INSERT INTO `admin_nqt_logs` VALUES ('1045', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 13:38:29');
INSERT INTO `admin_nqt_logs` VALUES ('1046', 'companies', '103', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:43:18');
INSERT INTO `admin_nqt_logs` VALUES ('1047', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 13:44:39');
INSERT INTO `admin_nqt_logs` VALUES ('1048', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:49');
INSERT INTO `admin_nqt_logs` VALUES ('1049', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:49');
INSERT INTO `admin_nqt_logs` VALUES ('1050', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:49');
INSERT INTO `admin_nqt_logs` VALUES ('1051', 'companies', '101', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 16:03:52');
INSERT INTO `admin_nqt_logs` VALUES ('1052', 'companies', '83', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 16:03:52');
INSERT INTO `admin_nqt_logs` VALUES ('1053', 'companies', '102', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-06 16:03:52');
INSERT INTO `admin_nqt_logs` VALUES ('1054', 'companies', '102', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:54');
INSERT INTO `admin_nqt_logs` VALUES ('1055', 'companies', '83', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:54');
INSERT INTO `admin_nqt_logs` VALUES ('1056', 'companies', '101', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-06 16:03:54');
INSERT INTO `admin_nqt_logs` VALUES ('1057', 'companies', '148', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 08:06:59');
INSERT INTO `admin_nqt_logs` VALUES ('1058', 'companies', '147', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 08:06:59');
INSERT INTO `admin_nqt_logs` VALUES ('1059', 'companies', '148', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-10-08 08:08:14');
INSERT INTO `admin_nqt_logs` VALUES ('1060', 'companies', '148', 'end_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 08:08:14');
INSERT INTO `admin_nqt_logs` VALUES ('1061', 'companies', '148', 'start_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 08:08:14');
INSERT INTO `admin_nqt_logs` VALUES ('1062', 'companies', '163', 'logo', 'Update', null, '2015/10/16f255b280e447366596c636b3c4b65d.jpg', 'admin', '::1', '2015-10-08 13:34:02');
INSERT INTO `admin_nqt_logs` VALUES ('1063', 'companies', '163', 'end_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 13:34:02');
INSERT INTO `admin_nqt_logs` VALUES ('1064', 'companies', '163', 'start_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 13:34:02');
INSERT INTO `admin_nqt_logs` VALUES ('1065', 'companies', '163', 'description', 'Update', '', '<img src=\"../../../assets/uploads/editor/0198f777f4d6758ff9d46fbb00cf1ac4.jpg\" alt=\"\" border=\"0\" style=\"margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;\" />', 'admin', '::1', '2015-10-08 13:36:39');
INSERT INTO `admin_nqt_logs` VALUES ('1066', 'companies', '162', 'end_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 13:55:37');
INSERT INTO `admin_nqt_logs` VALUES ('1067', 'companies', '162', 'start_date', 'Update', '0000-00-00 00:00:00', '2015-10-08 00:00:00', 'admin', '::1', '2015-10-08 13:55:37');
INSERT INTO `admin_nqt_logs` VALUES ('1068', 'companies', '162', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 14:04:38');
INSERT INTO `admin_nqt_logs` VALUES ('1069', 'companies', '163', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 14:04:39');
INSERT INTO `admin_nqt_logs` VALUES ('1070', 'companies', '163', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-08 14:04:40');
INSERT INTO `admin_nqt_logs` VALUES ('1071', 'companies', '162', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-08 14:04:41');
INSERT INTO `admin_nqt_logs` VALUES ('1072', 'companies', '162', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 14:04:42');
INSERT INTO `admin_nqt_logs` VALUES ('1073', 'companies', '163', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 14:04:42');
INSERT INTO `admin_nqt_logs` VALUES ('1074', 'companies', '163', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-08 14:11:23');
INSERT INTO `admin_nqt_logs` VALUES ('1075', 'companies', '163', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-08 14:11:36');
INSERT INTO `admin_nqt_logs` VALUES ('1076', 'companies', '164', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:13:16');
INSERT INTO `admin_nqt_logs` VALUES ('1077', 'companies', '164', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-08 14:15:49');
INSERT INTO `admin_nqt_logs` VALUES ('1078', 'companies', '162', 'status', 'update', '1', '0', 'admin', '::1', '2015-10-08 14:15:50');
INSERT INTO `admin_nqt_logs` VALUES ('1079', 'companies', '164', 'logo', 'Update', '', '2015/10/e9c6f2edd8c084099f3f1934781ccf75.png', 'admin', '::1', '2015-10-08 14:18:16');
INSERT INTO `admin_nqt_logs` VALUES ('1080', 'companies', '165', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:23:43');
INSERT INTO `admin_nqt_logs` VALUES ('1081', 'companies', '166', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:27:45');
INSERT INTO `admin_nqt_logs` VALUES ('1082', 'companies', '167', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:37:18');
INSERT INTO `admin_nqt_logs` VALUES ('1083', 'companies', '168', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:37:41');
INSERT INTO `admin_nqt_logs` VALUES ('1084', 'companies', '169', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:40:48');
INSERT INTO `admin_nqt_logs` VALUES ('1085', 'companies', '170', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:41:46');
INSERT INTO `admin_nqt_logs` VALUES ('1086', 'companies', '171', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-08 14:42:59');
INSERT INTO `admin_nqt_logs` VALUES ('1087', 'companies', '171', 'description', 'Update', '', '&lt;script>', 'admin', '::1', '2015-10-08 16:44:52');
INSERT INTO `admin_nqt_logs` VALUES ('1088', 'companies', '171', 'description', 'Update', '&lt;script>', '<script>\r\n//<![CDATA[\r\n\r\n//]]>\r\n</script>', 'admin', '::1', '2015-10-08 16:45:20');
INSERT INTO `admin_nqt_logs` VALUES ('1089', 'companies', '170', 'logo', 'Update', '2015/10/696aff23e44c217a9df2ec54fa9871b6.png', '2015/10/b5a28ad08d6ebe4072547ed38ca21724.jpg', 'admin', '::1', '2015-10-08 16:46:36');
INSERT INTO `admin_nqt_logs` VALUES ('1090', 'companies', '173', 'status', 'update', '0', '1', 'admin', '::1', '2015-10-14 13:25:11');
INSERT INTO `admin_nqt_logs` VALUES ('1091', 'companies', '173', 'forever', 'Update', '0', '1', 'admin', '::1', '2015-10-14 13:25:27');
INSERT INTO `admin_nqt_logs` VALUES ('1092', 'companies', '173', 'end_date', 'Update', '0000-00-00 00:00:00', '2015-10-14 00:00:00', 'admin', '::1', '2015-10-14 13:25:27');
INSERT INTO `admin_nqt_logs` VALUES ('1093', 'companies', '173', 'start_date', 'Update', '0000-00-00 00:00:00', '2015-10-14 00:00:00', 'admin', '::1', '2015-10-14 13:25:27');
INSERT INTO `admin_nqt_logs` VALUES ('1094', 'companies', '173', 'logo', 'Update', '', '2015/10/284c3320bcd8957919548fb653eb2a9a.jpg', 'admin', '::1', '2015-10-14 13:26:53');
INSERT INTO `admin_nqt_logs` VALUES ('1095', 'companies', '174', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-10-14 13:27:39');
INSERT INTO `admin_nqt_logs` VALUES ('1096', 'companies', '170', 'logo', 'Update', '2015/10/b5a28ad08d6ebe4072547ed38ca21724.jpg', '2015/10/15732483aa0ac6c8b1bf47ea92cc83c8.jpg', 'admin', '::1', '2015-10-16 10:53:57');
INSERT INTO `admin_nqt_logs` VALUES ('1097', 'companies', '170', 'logo', 'Update', '2015/10/15732483aa0ac6c8b1bf47ea92cc83c8.jpg', '2015/10/6d7eddb2c7ed126374d4df00b5062890.jpg', 'admin', '::1', '2015-10-16 13:31:21');
INSERT INTO `admin_nqt_logs` VALUES ('1098', 'companies', '171', 'logo', 'Update', '2015/10/7334a798edd02f1f2b5f5dc0a6caace1.jpg', '2015/10/b841d6e6b96629a31ece5bb7d59bf0d1.jpg', 'admin', '::1', '2015-10-16 13:33:21');
INSERT INTO `admin_nqt_logs` VALUES ('1099', 'companies', '170', 'logo', 'Update', '2015/10/6d7eddb2c7ed126374d4df00b5062890.jpg', '2015/10/239e9038348d6bf34fee97f6109c3722.jpg', 'admin', '::1', '2015-10-16 13:33:42');
INSERT INTO `admin_nqt_logs` VALUES ('1100', 'companies', '170', 'logo', 'Update', '2015/10/239e9038348d6bf34fee97f6109c3722.jpg', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', 'admin', '::1', '2015-10-16 13:34:02');
INSERT INTO `admin_nqt_logs` VALUES ('1101', 'companies', '177', 'Add new', 'Add new', '', '', 'admin', '::1', '2015-11-03 10:47:17');
INSERT INTO `admin_nqt_logs` VALUES ('1102', 'companies', '177', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:15');
INSERT INTO `admin_nqt_logs` VALUES ('1103', 'companies', '178', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:15');
INSERT INTO `admin_nqt_logs` VALUES ('1104', 'companies', '179', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:16');
INSERT INTO `admin_nqt_logs` VALUES ('1105', 'companies', '180', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:16');
INSERT INTO `admin_nqt_logs` VALUES ('1106', 'companies', '184', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:17');
INSERT INTO `admin_nqt_logs` VALUES ('1107', 'companies', '171', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:18');
INSERT INTO `admin_nqt_logs` VALUES ('1108', 'companies', '181', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:19');
INSERT INTO `admin_nqt_logs` VALUES ('1109', 'companies', '182', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:20');
INSERT INTO `admin_nqt_logs` VALUES ('1110', 'companies', '186', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:20');
INSERT INTO `admin_nqt_logs` VALUES ('1111', 'companies', '187', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:21');
INSERT INTO `admin_nqt_logs` VALUES ('1112', 'companies', '205', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:23');
INSERT INTO `admin_nqt_logs` VALUES ('1113', 'companies', '202', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:23');
INSERT INTO `admin_nqt_logs` VALUES ('1114', 'companies', '199', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:24');
INSERT INTO `admin_nqt_logs` VALUES ('1115', 'companies', '195', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:24');
INSERT INTO `admin_nqt_logs` VALUES ('1116', 'companies', '193', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:25');
INSERT INTO `admin_nqt_logs` VALUES ('1117', 'companies', '192', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:27');
INSERT INTO `admin_nqt_logs` VALUES ('1118', 'companies', '191', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:27');
INSERT INTO `admin_nqt_logs` VALUES ('1119', 'companies', '190', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:28');
INSERT INTO `admin_nqt_logs` VALUES ('1120', 'companies', '189', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:28');
INSERT INTO `admin_nqt_logs` VALUES ('1121', 'companies', '188', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:29');
INSERT INTO `admin_nqt_logs` VALUES ('1122', 'companies', '203', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:31');
INSERT INTO `admin_nqt_logs` VALUES ('1123', 'companies', '201', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:32');
INSERT INTO `admin_nqt_logs` VALUES ('1124', 'companies', '200', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:32');
INSERT INTO `admin_nqt_logs` VALUES ('1125', 'companies', '198', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:33');
INSERT INTO `admin_nqt_logs` VALUES ('1126', 'companies', '197', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:34');
INSERT INTO `admin_nqt_logs` VALUES ('1127', 'companies', '196', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:34');
INSERT INTO `admin_nqt_logs` VALUES ('1128', 'companies', '194', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:37');
INSERT INTO `admin_nqt_logs` VALUES ('1129', 'companies', '185', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:38');
INSERT INTO `admin_nqt_logs` VALUES ('1130', 'companies', '183', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:39');
INSERT INTO `admin_nqt_logs` VALUES ('1131', 'companies', '170', 'status', 'update', '1', '0', 'admin', '::1', '2015-11-03 11:10:40');
INSERT INTO `admin_nqt_logs` VALUES ('1132', 'companies', '203', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 11:10:53');
INSERT INTO `admin_nqt_logs` VALUES ('1133', 'companies', '201', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 11:10:54');
INSERT INTO `admin_nqt_logs` VALUES ('1134', 'companies', '200', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:18');
INSERT INTO `admin_nqt_logs` VALUES ('1135', 'companies', '198', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:19');
INSERT INTO `admin_nqt_logs` VALUES ('1136', 'companies', '197', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:19');
INSERT INTO `admin_nqt_logs` VALUES ('1137', 'companies', '196', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:20');
INSERT INTO `admin_nqt_logs` VALUES ('1138', 'companies', '194', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:21');
INSERT INTO `admin_nqt_logs` VALUES ('1139', 'companies', '185', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:22');
INSERT INTO `admin_nqt_logs` VALUES ('1140', 'companies', '183', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:22');
INSERT INTO `admin_nqt_logs` VALUES ('1141', 'companies', '170', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:23');
INSERT INTO `admin_nqt_logs` VALUES ('1142', 'companies', '205', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:27');
INSERT INTO `admin_nqt_logs` VALUES ('1143', 'companies', '202', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:28');
INSERT INTO `admin_nqt_logs` VALUES ('1144', 'companies', '199', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:28');
INSERT INTO `admin_nqt_logs` VALUES ('1145', 'companies', '195', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:29');
INSERT INTO `admin_nqt_logs` VALUES ('1146', 'companies', '193', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:29');
INSERT INTO `admin_nqt_logs` VALUES ('1147', 'companies', '192', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:30');
INSERT INTO `admin_nqt_logs` VALUES ('1148', 'companies', '191', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:31');
INSERT INTO `admin_nqt_logs` VALUES ('1149', 'companies', '190', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:31');
INSERT INTO `admin_nqt_logs` VALUES ('1150', 'companies', '189', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:32');
INSERT INTO `admin_nqt_logs` VALUES ('1151', 'companies', '188', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:33');
INSERT INTO `admin_nqt_logs` VALUES ('1152', 'companies', '187', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:36');
INSERT INTO `admin_nqt_logs` VALUES ('1153', 'companies', '186', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:37');
INSERT INTO `admin_nqt_logs` VALUES ('1154', 'companies', '182', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:37');
INSERT INTO `admin_nqt_logs` VALUES ('1155', 'companies', '181', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:38');
INSERT INTO `admin_nqt_logs` VALUES ('1156', 'companies', '171', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:39');
INSERT INTO `admin_nqt_logs` VALUES ('1157', 'companies', '184', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:40');
INSERT INTO `admin_nqt_logs` VALUES ('1158', 'companies', '180', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:40');
INSERT INTO `admin_nqt_logs` VALUES ('1159', 'companies', '179', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:42');
INSERT INTO `admin_nqt_logs` VALUES ('1160', 'companies', '178', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:42');
INSERT INTO `admin_nqt_logs` VALUES ('1161', 'companies', '177', 'status', 'update', '0', '1', 'admin', '::1', '2015-11-03 12:06:43');

-- ----------------------------
-- Table structure for admin_nqt_modules
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_modules`;
CREATE TABLE `admin_nqt_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_function` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_modules
-- ----------------------------
INSERT INTO `admin_nqt_modules` VALUES ('1', 'Manager Account Group', 'admincp_account_groups', '1', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('2', 'Manager Account', 'admincp_accounts', '1', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('3', 'Manager Module', 'admincp_modules', '0', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('4', 'Manager Logs', 'admincp_logs', '1', '2012-08-16 15:53:42');
INSERT INTO `admin_nqt_modules` VALUES ('84', 'Static pages', 'static_pages', '1', '2015-06-14 19:36:38');
INSERT INTO `admin_nqt_modules` VALUES ('154', 'Companies', 'companies', '1', '2015-07-31 10:30:11');
INSERT INTO `admin_nqt_modules` VALUES ('159', 'Members', 'members', '1', '2015-08-03 10:42:35');
INSERT INTO `admin_nqt_modules` VALUES ('164', 'Job recruiments', 'job_recruiments', '1', '2015-08-04 16:13:11');
INSERT INTO `admin_nqt_modules` VALUES ('203', 'Detail member', 'detail_member', '1', '2015-09-18 14:25:13');

-- ----------------------------
-- Table structure for admin_nqt_settings
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_settings`;
CREATE TABLE `admin_nqt_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_settings
-- ----------------------------
INSERT INTO `admin_nqt_settings` VALUES ('1', 'mail-admin', 'hongnga@jv-it.com.vn', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('2', 'name-mail-admin', 'Admin Brilliant English School', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('3', 'subject', '【Brilliant English School】Contact Information.', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('4', 'seo-title', 'ブリリアント英会話inホーチミン | Brilliant English Vietnam', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('5', 'seo-meta-keyword', '英会話,英語教室,ホーチミン,ベトナム,スクール', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('6', 'seo-meta-description', 'こんな英会話スクールを探していた！時間も、場所も自由、直前予約も可能、空いた時間に英語力アップ！わたしの英語教室、「ブリリアント英会話」', '2015-09-25 14:33:24');
INSERT INTO `admin_nqt_settings` VALUES ('8', 'timeout-mail', '5', '2015-09-25 14:33:24');

-- ----------------------------
-- Table structure for admin_nqt_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_nqt_users`;
CREATE TABLE `admin_nqt_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `custom_permission` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_nqt_users
-- ----------------------------
INSERT INTO `admin_nqt_users` VALUES ('1', 'root', '53fab80925e21d959402658124f71c36', '1', '116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '0', '1', '2012-08-28 14:52:42');
INSERT INTO `admin_nqt_users` VALUES ('2', 'admin', 'af03798e4f9010c54d2eb6f386124f7e', '1', '116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '0', '1', '2012-08-28 14:52:59');
INSERT INTO `admin_nqt_users` VALUES ('27', 'kimcuu1992', 'e10adc3949ba59abbe56e057f20f883e', '1', '116|rwd,125|rwd,36|rwd,2|rwd,1|rwd,4|rwd,3|rwd,136|rwd,86|rwd,99|rwd,90|rwd,130|rwd,113|rwd,138|rwd,151|rwd,154|rwd,159|rwd,164|rwd', '1', '1', '2015-09-23 11:25:18');

-- ----------------------------
-- Table structure for jv-it_banners
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_banners`;
CREATE TABLE `jv-it_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `order` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_banners
-- ----------------------------
INSERT INTO `jv-it_banners` VALUES ('64', 'https://www.youtube.com/watch?v=-LLkAchJaj0&list=RDGMEMQ1dJ7wXfLlqCjwV0xfSNbAVM9V4RXwdKuwQ&index=5', '2015/07/2a96756d0b26c36fcfa25d314de20cac.jpg', 'test', '1', '1', '2015-07-30 10:56:07');
INSERT INTO `jv-it_banners` VALUES ('65', 'http://mp3.zing.vn/bai-hat/Again-Again-2PM/ZWZ9ZAWI.html', '2015/07/cce9c37a68f920bc7e544be9af994013.jpg', 'test', '2', '1', '2015-07-31 08:53:32');

-- ----------------------------
-- Table structure for jv-it_companies
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_companies`;
CREATE TABLE `jv-it_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `katakana` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `description` text,
  `order` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `forever` tinyint(1) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `first` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_companies
-- ----------------------------
INSERT INTO `jv-it_companies` VALUES ('184', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('170', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('171', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('177', 'kimcuuf@jv-it.com.vn', 'kim cuu', 'ミタニ', 'test', 'c', '2015/11/9a5e349b5d8d6e04d61839687391051a.jpg', '', '0', '1', '2015-11-03 10:47:17', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('178', 'kimcuuf@jv-it.com.vn', 'kim cuu', 'ミタニ', 'test', 'c', '2015/11/9a5e349b5d8d6e04d61839687391051a.jpg', '', '0', '1', '2015-11-03 10:47:17', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('179', 'kimcuuf@jv-it.com.vn', 'kim cuu', 'ミタニ', 'test', 'c', '2015/11/9a5e349b5d8d6e04d61839687391051a.jpg', '', '0', '1', '2015-11-03 10:47:17', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('180', 'kimcuuf@jv-it.com.vn', 'kim cuu', 'ミタニ', 'test', 'c', '2015/11/9a5e349b5d8d6e04d61839687391051a.jpg', '', '0', '1', '2015-11-03 10:47:17', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('181', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('182', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('183', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('185', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('186', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('187', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('188', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('189', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('190', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('191', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('192', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('193', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('194', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('195', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('196', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('197', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('198', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('199', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('200', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('201', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('202', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `jv-it_companies` VALUES ('203', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('204', 'kimcuu@jv-it.com.vn', '三谷ミタニ', 'カ', 'test', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:41:46', '1', null, null, '1');
INSERT INTO `jv-it_companies` VALUES ('205', 'kimcuu1992@gmail.com', 'kimcuu', 'カ', 'jv-it', 'test', '2015/10/9958a9be28ff850ba8591732d1526d36.jpg', '', '0', '1', '2015-10-08 14:42:59', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for jv-it_detail_members
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_detail_members`;
CREATE TABLE `jv-it_detail_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `time_send_mail` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jv-it_detail_members
-- ----------------------------
INSERT INTO `jv-it_detail_members` VALUES ('155', '168', '130', '83', '2015-09-25 14:28:54', '1', '0000-00-00 00:00:00');
INSERT INTO `jv-it_detail_members` VALUES ('156', '169', '131', '83', '2015-09-25 14:31:52', '0', '0000-00-00 00:00:00');
INSERT INTO `jv-it_detail_members` VALUES ('157', '170', '132', '83', '2015-09-25 14:33:46', '0', '0000-00-00 00:00:00');
INSERT INTO `jv-it_detail_members` VALUES ('158', '171', '133', '83', '2015-09-25 14:36:03', '0', '0000-00-00 00:00:00');
INSERT INTO `jv-it_detail_members` VALUES ('159', '173', '113', '83', '2015-09-25 14:37:59', '0', '2015-09-25 14:42:59');
INSERT INTO `jv-it_detail_members` VALUES ('208', '217', '113', '102', '2015-10-07 16:12:10', '0', '2015-10-07 16:12:10');
INSERT INTO `jv-it_detail_members` VALUES ('209', '219', '168', '102', '2015-10-07 16:15:36', '0', '2015-10-07 16:15:36');
INSERT INTO `jv-it_detail_members` VALUES ('210', '221', '170', '83', '2015-10-07 16:16:42', '0', '2015-10-07 16:21:42');
INSERT INTO `jv-it_detail_members` VALUES ('211', '221', '170', '101', '2015-10-07 16:16:42', '0', '2015-10-07 16:16:42');
INSERT INTO `jv-it_detail_members` VALUES ('212', '221', '170', '102', '2015-10-07 16:16:43', '0', '2015-10-07 16:16:43');
INSERT INTO `jv-it_detail_members` VALUES ('213', '227', '176', '102', '2015-10-07 16:20:38', '0', '2015-10-07 16:20:38');
INSERT INTO `jv-it_detail_members` VALUES ('214', '237', '186', '102', '2015-10-07 16:27:28', '0', '2015-10-07 16:27:28');
INSERT INTO `jv-it_detail_members` VALUES ('215', '244', '193', '83', '2015-10-07 16:31:28', '0', '2015-10-07 16:36:28');
INSERT INTO `jv-it_detail_members` VALUES ('216', '244', '193', '101', '2015-10-07 16:31:28', '0', '2015-10-07 16:31:28');
INSERT INTO `jv-it_detail_members` VALUES ('217', '244', '193', '102', '2015-10-07 16:31:28', '0', '2015-10-07 16:31:28');
INSERT INTO `jv-it_detail_members` VALUES ('218', '249', '198', '83', '2015-10-07 16:33:23', '0', '2015-10-07 16:38:23');
INSERT INTO `jv-it_detail_members` VALUES ('219', '249', '198', '101', '2015-10-07 16:33:23', '0', '2015-10-07 16:33:23');
INSERT INTO `jv-it_detail_members` VALUES ('220', '249', '198', '102', '2015-10-07 16:33:24', '0', '2015-10-07 16:33:24');
INSERT INTO `jv-it_detail_members` VALUES ('221', '280', '201', '170', '2015-10-09 14:42:43', '0', '2015-10-09 14:42:43');
INSERT INTO `jv-it_detail_members` VALUES ('222', '280', '201', '171', '2015-10-09 14:42:44', '0', '2015-10-09 14:42:44');
INSERT INTO `jv-it_detail_members` VALUES ('223', '281', '113', '170', '2015-10-09 15:11:49', '0', '2015-10-09 15:11:49');
INSERT INTO `jv-it_detail_members` VALUES ('224', '281', '113', '171', '2015-10-09 15:11:49', '0', '2015-10-09 15:11:49');
INSERT INTO `jv-it_detail_members` VALUES ('225', '282', '113', '170', '2015-10-14 13:33:20', '0', '2015-10-14 13:33:20');
INSERT INTO `jv-it_detail_members` VALUES ('226', '282', '113', '171', '2015-10-14 13:33:21', '0', '2015-10-14 13:33:21');
INSERT INTO `jv-it_detail_members` VALUES ('227', '282', '113', '173', '2015-10-14 13:33:21', '0', '2015-10-14 13:38:21');
INSERT INTO `jv-it_detail_members` VALUES ('228', '282', '113', '174', '2015-10-14 13:33:21', '0', '2015-10-14 13:33:21');
INSERT INTO `jv-it_detail_members` VALUES ('229', '285', '113', '170', '2015-10-14 13:39:25', '0', '2015-10-14 13:39:25');
INSERT INTO `jv-it_detail_members` VALUES ('230', '285', '113', '171', '2015-10-14 13:39:25', '0', '2015-10-14 13:39:25');
INSERT INTO `jv-it_detail_members` VALUES ('231', '285', '113', '173', '2015-10-14 13:39:25', '0', '2015-10-14 13:44:25');
INSERT INTO `jv-it_detail_members` VALUES ('232', '285', '113', '174', '2015-10-14 13:39:25', '0', '2015-10-14 13:39:25');
INSERT INTO `jv-it_detail_members` VALUES ('233', '286', '113', '170', '2015-10-14 13:43:22', '0', '2015-10-14 13:43:22');
INSERT INTO `jv-it_detail_members` VALUES ('234', '286', '113', '171', '2015-10-14 13:43:22', '0', '2015-10-14 13:43:22');
INSERT INTO `jv-it_detail_members` VALUES ('235', '286', '113', '173', '2015-10-14 13:43:22', '0', '2015-10-14 13:48:22');
INSERT INTO `jv-it_detail_members` VALUES ('236', '286', '113', '174', '2015-10-14 13:43:23', '0', '2015-10-14 13:43:23');
INSERT INTO `jv-it_detail_members` VALUES ('237', '288', '113', '170', '2015-10-14 13:50:46', '0', '2015-10-14 13:50:46');
INSERT INTO `jv-it_detail_members` VALUES ('238', '288', '113', '171', '2015-10-14 13:50:46', '0', '2015-10-14 13:50:46');
INSERT INTO `jv-it_detail_members` VALUES ('239', '288', '113', '173', '2015-10-14 13:50:46', '0', '2015-10-14 13:55:46');
INSERT INTO `jv-it_detail_members` VALUES ('240', '288', '113', '174', '2015-10-14 13:50:46', '0', '2015-10-14 13:50:46');
INSERT INTO `jv-it_detail_members` VALUES ('241', '289', '113', '170', '2015-10-14 13:51:38', '0', '2015-10-14 13:51:38');
INSERT INTO `jv-it_detail_members` VALUES ('242', '289', '113', '171', '2015-10-14 13:51:38', '0', '2015-10-14 13:51:38');
INSERT INTO `jv-it_detail_members` VALUES ('243', '289', '113', '173', '2015-10-14 13:51:38', '0', '2015-10-14 13:56:38');
INSERT INTO `jv-it_detail_members` VALUES ('244', '289', '113', '174', '2015-10-14 13:51:38', '0', '2015-10-14 13:51:38');
INSERT INTO `jv-it_detail_members` VALUES ('245', '291', '113', '170', '2015-10-14 13:52:11', '0', '2015-10-14 13:52:11');
INSERT INTO `jv-it_detail_members` VALUES ('246', '291', '113', '171', '2015-10-14 13:52:11', '0', '2015-10-14 13:52:11');
INSERT INTO `jv-it_detail_members` VALUES ('247', '291', '113', '173', '2015-10-14 13:52:11', '0', '2015-10-14 13:57:11');
INSERT INTO `jv-it_detail_members` VALUES ('248', '291', '113', '174', '2015-10-14 13:52:11', '0', '2015-10-14 13:52:11');

-- ----------------------------
-- Table structure for jv-it_job_recruiments
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_job_recruiments`;
CREATE TABLE `jv-it_job_recruiments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `member_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=292 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_job_recruiments
-- ----------------------------
INSERT INTO `jv-it_job_recruiments` VALUES ('1', '■ 依頼内容<br />\r\ntest<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '1', '1', '2015-08-04 16:57:51');
INSERT INTO `jv-it_job_recruiments` VALUES ('123', '■ 依頼内容<br />\r\ntest<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '60', '1', '2015-08-28 10:21:15');
INSERT INTO `jv-it_job_recruiments` VALUES ('76', '■ 依頼内容<br />\r\ntest<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。<br />\r\n       ', '69', '1', '2015-08-26 09:02:49');
INSERT INTO `jv-it_job_recruiments` VALUES ('131', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '68', '1', '2015-08-28 13:48:52');
INSERT INTO `jv-it_job_recruiments` VALUES ('132', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '95', '1', '2015-09-06 18:52:17');
INSERT INTO `jv-it_job_recruiments` VALUES ('133', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '96', '1', '2015-09-07 10:07:20');
INSERT INTO `jv-it_job_recruiments` VALUES ('134', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '97', '1', '2015-09-07 10:12:01');
INSERT INTO `jv-it_job_recruiments` VALUES ('135', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '98', '1', '2015-09-07 10:25:39');
INSERT INTO `jv-it_job_recruiments` VALUES ('136', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。            ', '99', '1', '2015-09-07 14:56:37');
INSERT INTO `jv-it_job_recruiments` VALUES ('137', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '100', '1', '2015-09-11 21:01:37');
INSERT INTO `jv-it_job_recruiments` VALUES ('138', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '101', '1', '2015-09-12 00:06:10');
INSERT INTO `jv-it_job_recruiments` VALUES ('139', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '102', '1', '2015-09-12 00:12:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('140', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '103', '1', '2015-09-12 00:21:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('141', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '104', '1', '2015-09-12 00:25:15');
INSERT INTO `jv-it_job_recruiments` VALUES ('142', null, '105', '1', '2015-09-12 00:37:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('143', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '108', '1', '2015-09-16 21:56:37');
INSERT INTO `jv-it_job_recruiments` VALUES ('144', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '110', '1', '2015-09-17 09:49:22');
INSERT INTO `jv-it_job_recruiments` VALUES ('145', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '111', '1', '2015-09-17 09:50:17');
INSERT INTO `jv-it_job_recruiments` VALUES ('146', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '112', '1', '2015-09-18 08:02:37');
INSERT INTO `jv-it_job_recruiments` VALUES ('147', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。                   ', '113', '1', '2015-09-19 12:37:26');
INSERT INTO `jv-it_job_recruiments` VALUES ('148', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:46:34');
INSERT INTO `jv-it_job_recruiments` VALUES ('149', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:51:49');
INSERT INTO `jv-it_job_recruiments` VALUES ('150', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:51:55');
INSERT INTO `jv-it_job_recruiments` VALUES ('151', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:52:04');
INSERT INTO `jv-it_job_recruiments` VALUES ('152', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:52:40');
INSERT INTO `jv-it_job_recruiments` VALUES ('153', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:53:44');
INSERT INTO `jv-it_job_recruiments` VALUES ('154', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '1', '2015-09-19 12:54:48');
INSERT INTO `jv-it_job_recruiments` VALUES ('155', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '114', '1', '2015-09-21 16:30:56');
INSERT INTO `jv-it_job_recruiments` VALUES ('156', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '115', '1', '2015-09-21 16:52:29');
INSERT INTO `jv-it_job_recruiments` VALUES ('157', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '116', '1', '2015-09-22 17:01:40');
INSERT INTO `jv-it_job_recruiments` VALUES ('158', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '117', '1', '2015-09-23 14:41:31');
INSERT INTO `jv-it_job_recruiments` VALUES ('159', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '121', '1', '2015-09-24 13:45:39');
INSERT INTO `jv-it_job_recruiments` VALUES ('160', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '113', '1', '2015-09-24 13:59:10');
INSERT INTO `jv-it_job_recruiments` VALUES ('161', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '122', '1', '2015-09-24 14:23:59');
INSERT INTO `jv-it_job_recruiments` VALUES ('162', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '123', '1', '2015-09-25 10:59:06');
INSERT INTO `jv-it_job_recruiments` VALUES ('163', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '124', '1', '2015-09-25 11:00:26');
INSERT INTO `jv-it_job_recruiments` VALUES ('164', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '125', '1', '2015-09-25 11:03:18');
INSERT INTO `jv-it_job_recruiments` VALUES ('165', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '126', '1', '2015-09-25 11:21:19');
INSERT INTO `jv-it_job_recruiments` VALUES ('166', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '127', '1', '2015-09-25 11:25:01');
INSERT INTO `jv-it_job_recruiments` VALUES ('167', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '128', '1', '2015-09-25 11:26:22');
INSERT INTO `jv-it_job_recruiments` VALUES ('168', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '130', '1', '2015-09-25 14:28:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('169', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '131', '1', '2015-09-25 14:31:52');
INSERT INTO `jv-it_job_recruiments` VALUES ('170', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '132', '1', '2015-09-25 14:33:46');
INSERT INTO `jv-it_job_recruiments` VALUES ('171', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '133', '1', '2015-09-25 14:36:03');
INSERT INTO `jv-it_job_recruiments` VALUES ('172', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '134', '1', '2015-09-25 14:36:48');
INSERT INTO `jv-it_job_recruiments` VALUES ('173', 'aaa', '135', '1', '2015-09-25 14:37:59');
INSERT INTO `jv-it_job_recruiments` VALUES ('174', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '136', '0', '2015-10-07 14:59:07');
INSERT INTO `jv-it_job_recruiments` VALUES ('175', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '137', '0', '2015-10-07 15:01:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('176', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '138', '0', '2015-10-07 15:03:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('177', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '139', '0', '2015-10-07 15:04:56');
INSERT INTO `jv-it_job_recruiments` VALUES ('178', null, '140', '0', '2015-10-07 15:04:57');
INSERT INTO `jv-it_job_recruiments` VALUES ('179', null, '141', '0', '2015-10-07 15:05:19');
INSERT INTO `jv-it_job_recruiments` VALUES ('180', null, '142', '0', '2015-10-07 15:05:28');
INSERT INTO `jv-it_job_recruiments` VALUES ('181', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '143', '0', '2015-10-07 15:07:59');
INSERT INTO `jv-it_job_recruiments` VALUES ('182', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '144', '0', '2015-10-07 15:10:24');
INSERT INTO `jv-it_job_recruiments` VALUES ('183', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '145', '0', '2015-10-07 15:11:29');
INSERT INTO `jv-it_job_recruiments` VALUES ('184', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '146', '0', '2015-10-07 15:15:18');
INSERT INTO `jv-it_job_recruiments` VALUES ('185', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '147', '0', '2015-10-07 15:19:10');
INSERT INTO `jv-it_job_recruiments` VALUES ('186', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '148', '0', '2015-10-07 15:21:42');
INSERT INTO `jv-it_job_recruiments` VALUES ('187', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '149', '0', '2015-10-07 15:24:13');
INSERT INTO `jv-it_job_recruiments` VALUES ('188', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '150', '0', '2015-10-07 15:24:53');
INSERT INTO `jv-it_job_recruiments` VALUES ('189', null, '151', '0', '2015-10-07 15:24:53');
INSERT INTO `jv-it_job_recruiments` VALUES ('190', null, '152', '0', '2015-10-07 15:24:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('191', null, '153', '0', '2015-10-07 15:24:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('192', null, '154', '0', '2015-10-07 15:24:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('193', null, '155', '0', '2015-10-07 15:24:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('194', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '156', '0', '2015-10-07 15:26:09');
INSERT INTO `jv-it_job_recruiments` VALUES ('195', null, '157', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('196', null, '158', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('197', null, '159', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('198', null, '160', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('199', null, '161', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('200', null, '162', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('201', null, '163', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('202', null, '164', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('203', null, '165', '0', '2015-10-07 15:26:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('204', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '166', '0', '2015-10-07 15:27:04');
INSERT INTO `jv-it_job_recruiments` VALUES ('205', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '167', '0', '2015-10-07 15:28:35');
INSERT INTO `jv-it_job_recruiments` VALUES ('206', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '113', '0', '2015-10-07 16:05:46');
INSERT INTO `jv-it_job_recruiments` VALUES ('207', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '113', '0', '2015-10-07 16:05:47');
INSERT INTO `jv-it_job_recruiments` VALUES ('208', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '113', '0', '2015-10-07 16:05:51');
INSERT INTO `jv-it_job_recruiments` VALUES ('209', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:07:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('210', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:07:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('211', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:07:34');
INSERT INTO `jv-it_job_recruiments` VALUES ('212', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:09:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('213', '', '113', '0', '2015-10-07 16:09:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('214', null, '113', '0', '2015-10-07 16:10:43');
INSERT INTO `jv-it_job_recruiments` VALUES ('215', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:11:30');
INSERT INTO `jv-it_job_recruiments` VALUES ('216', '', '113', '0', '2015-10-07 16:11:31');
INSERT INTO `jv-it_job_recruiments` VALUES ('217', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-07 16:12:10');
INSERT INTO `jv-it_job_recruiments` VALUES ('218', '', '113', '0', '2015-10-07 16:12:11');
INSERT INTO `jv-it_job_recruiments` VALUES ('219', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '168', '0', '2015-10-07 16:15:36');
INSERT INTO `jv-it_job_recruiments` VALUES ('220', null, '169', '0', '2015-10-07 16:15:36');
INSERT INTO `jv-it_job_recruiments` VALUES ('221', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '170', '0', '2015-10-07 16:16:42');
INSERT INTO `jv-it_job_recruiments` VALUES ('222', null, '171', '0', '2015-10-07 16:16:43');
INSERT INTO `jv-it_job_recruiments` VALUES ('223', null, '172', '0', '2015-10-07 16:17:05');
INSERT INTO `jv-it_job_recruiments` VALUES ('224', null, '173', '0', '2015-10-07 16:17:05');
INSERT INTO `jv-it_job_recruiments` VALUES ('225', null, '174', '0', '2015-10-07 16:17:16');
INSERT INTO `jv-it_job_recruiments` VALUES ('226', null, '175', '0', '2015-10-07 16:17:16');
INSERT INTO `jv-it_job_recruiments` VALUES ('227', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '176', '0', '2015-10-07 16:20:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('228', null, '177', '0', '2015-10-07 16:20:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('229', null, '178', '0', '2015-10-07 16:21:39');
INSERT INTO `jv-it_job_recruiments` VALUES ('230', null, '179', '0', '2015-10-07 16:21:39');
INSERT INTO `jv-it_job_recruiments` VALUES ('231', null, '180', '0', '2015-10-07 16:22:34');
INSERT INTO `jv-it_job_recruiments` VALUES ('232', null, '181', '0', '2015-10-07 16:22:34');
INSERT INTO `jv-it_job_recruiments` VALUES ('233', null, '182', '0', '2015-10-07 16:24:08');
INSERT INTO `jv-it_job_recruiments` VALUES ('234', null, '183', '0', '2015-10-07 16:25:09');
INSERT INTO `jv-it_job_recruiments` VALUES ('235', null, '184', '0', '2015-10-07 16:26:12');
INSERT INTO `jv-it_job_recruiments` VALUES ('236', null, '185', '0', '2015-10-07 16:26:12');
INSERT INTO `jv-it_job_recruiments` VALUES ('237', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '186', '0', '2015-10-07 16:27:28');
INSERT INTO `jv-it_job_recruiments` VALUES ('238', null, '187', '0', '2015-10-07 16:27:30');
INSERT INTO `jv-it_job_recruiments` VALUES ('239', null, '188', '0', '2015-10-07 16:27:30');
INSERT INTO `jv-it_job_recruiments` VALUES ('240', null, '189', '0', '2015-10-07 16:27:48');
INSERT INTO `jv-it_job_recruiments` VALUES ('241', null, '190', '0', '2015-10-07 16:27:51');
INSERT INTO `jv-it_job_recruiments` VALUES ('242', null, '191', '0', '2015-10-07 16:27:53');
INSERT INTO `jv-it_job_recruiments` VALUES ('243', null, '192', '0', '2015-10-07 16:27:54');
INSERT INTO `jv-it_job_recruiments` VALUES ('244', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '193', '0', '2015-10-07 16:31:28');
INSERT INTO `jv-it_job_recruiments` VALUES ('245', null, '194', '0', '2015-10-07 16:31:30');
INSERT INTO `jv-it_job_recruiments` VALUES ('246', null, '195', '0', '2015-10-07 16:31:32');
INSERT INTO `jv-it_job_recruiments` VALUES ('247', null, '196', '0', '2015-10-07 16:31:37');
INSERT INTO `jv-it_job_recruiments` VALUES ('248', null, '197', '0', '2015-10-07 16:31:39');
INSERT INTO `jv-it_job_recruiments` VALUES ('249', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '198', '0', '2015-10-07 16:33:23');
INSERT INTO `jv-it_job_recruiments` VALUES ('250', null, '199', '0', '2015-10-07 16:33:26');
INSERT INTO `jv-it_job_recruiments` VALUES ('251', null, '200', '0', '2015-10-07 16:33:26');
INSERT INTO `jv-it_job_recruiments` VALUES ('252', '', '113', '0', '2015-10-08 07:57:47');
INSERT INTO `jv-it_job_recruiments` VALUES ('253', null, '113', '0', '2015-10-08 07:57:48');
INSERT INTO `jv-it_job_recruiments` VALUES ('254', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-08 07:58:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('255', '', '113', '0', '2015-10-08 07:58:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('256', '', '113', '0', '2015-10-08 07:58:49');
INSERT INTO `jv-it_job_recruiments` VALUES ('257', '', '113', '0', '2015-10-08 07:59:00');
INSERT INTO `jv-it_job_recruiments` VALUES ('258', '', '113', '0', '2015-10-08 07:59:57');
INSERT INTO `jv-it_job_recruiments` VALUES ('259', null, '113', '0', '2015-10-08 07:59:57');
INSERT INTO `jv-it_job_recruiments` VALUES ('260', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-08 08:00:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('261', '', '113', '0', '2015-10-08 08:00:34');
INSERT INTO `jv-it_job_recruiments` VALUES ('262', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-08 08:00:53');
INSERT INTO `jv-it_job_recruiments` VALUES ('263', '', '113', '0', '2015-10-08 08:00:53');
INSERT INTO `jv-it_job_recruiments` VALUES ('264', '', '113', '0', '2015-10-08 08:01:05');
INSERT INTO `jv-it_job_recruiments` VALUES ('265', '', '113', '0', '2015-10-08 08:01:08');
INSERT INTO `jv-it_job_recruiments` VALUES ('266', '', '113', '0', '2015-10-08 08:01:09');
INSERT INTO `jv-it_job_recruiments` VALUES ('267', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-08 08:08:32');
INSERT INTO `jv-it_job_recruiments` VALUES ('268', null, '113', '0', '2015-10-08 08:08:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('269', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-08 08:09:32');
INSERT INTO `jv-it_job_recruiments` VALUES ('270', null, '113', '0', '2015-10-08 08:09:32');
INSERT INTO `jv-it_job_recruiments` VALUES ('271', '', '113', '0', '2015-10-08 08:10:14');
INSERT INTO `jv-it_job_recruiments` VALUES ('272', null, '113', '0', '2015-10-08 08:10:15');
INSERT INTO `jv-it_job_recruiments` VALUES ('273', null, '113', '0', '2015-10-08 08:10:24');
INSERT INTO `jv-it_job_recruiments` VALUES ('274', '', '113', '0', '2015-10-08 09:00:12');
INSERT INTO `jv-it_job_recruiments` VALUES ('275', null, '113', '0', '2015-10-08 09:00:13');
INSERT INTO `jv-it_job_recruiments` VALUES ('276', '', '113', '0', '2015-10-08 09:01:13');
INSERT INTO `jv-it_job_recruiments` VALUES ('277', null, '113', '0', '2015-10-08 09:01:13');
INSERT INTO `jv-it_job_recruiments` VALUES ('278', null, '113', '0', '2015-10-08 09:32:56');
INSERT INTO `jv-it_job_recruiments` VALUES ('279', null, '113', '0', '2015-10-08 09:32:59');
INSERT INTO `jv-it_job_recruiments` VALUES ('280', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '201', '0', '2015-10-09 14:42:43');
INSERT INTO `jv-it_job_recruiments` VALUES ('281', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。     ', '113', '0', '2015-10-09 15:11:49');
INSERT INTO `jv-it_job_recruiments` VALUES ('282', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:33:20');
INSERT INTO `jv-it_job_recruiments` VALUES ('283', null, '113', '0', '2015-10-14 13:33:27');
INSERT INTO `jv-it_job_recruiments` VALUES ('284', null, '113', '0', '2015-10-14 13:33:33');
INSERT INTO `jv-it_job_recruiments` VALUES ('285', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:39:25');
INSERT INTO `jv-it_job_recruiments` VALUES ('286', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:43:22');
INSERT INTO `jv-it_job_recruiments` VALUES ('287', null, '113', '0', '2015-10-14 13:43:25');
INSERT INTO `jv-it_job_recruiments` VALUES ('288', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:50:46');
INSERT INTO `jv-it_job_recruiments` VALUES ('289', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:51:38');
INSERT INTO `jv-it_job_recruiments` VALUES ('290', null, '113', '0', '2015-10-14 13:51:40');
INSERT INTO `jv-it_job_recruiments` VALUES ('291', '■ 依頼内容<br />\r\n経理を急募しております。<br />\r\n<br />\r\n■ 詳細<br />\r\n・ 職種：経理<br />\r\n・ 給与：400～1000USD<br />\r\n・ 資格：アカウンタント<br />\r\n・ 優遇：会社での経理経験<br />\r\n・ 日本語能力：N2以上<br />\r\n・ 英語能力：TOEIC500点以上<br />\r\n・その他語学：中国語ができると尚良い<br />\r\n・勤務地：ホーチミン事務所<br />\r\n・勤務開始日：早ければ早いほど良い。<br />\r\n<br />\r\n■面接可能日：<br />\r\n8月28日〜9月10日、8:30～16:00<br />\r\n<br />\r\n■その他備考：<br />\r\n上記条件で、出来るだけ早く面接設定頂ければ助かります。', '113', '0', '2015-10-14 13:52:11');

-- ----------------------------
-- Table structure for jv-it_members
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_members`;
CREATE TABLE `jv-it_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_members
-- ----------------------------
INSERT INTO `jv-it_members` VALUES ('113', 'kimcuu', 'jv-it', 'kimcuu1992@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0', '1', '2015-09-18 15:02:21');
INSERT INTO `jv-it_members` VALUES ('133', 'a', 'a', 'kimcuu@jv-it.com.vn', 'e10adc3949ba59abbe56e057f20f883e', '0', '1', '2015-09-25 14:36:03');

-- ----------------------------
-- Table structure for jv-it_static_pages
-- ----------------------------
DROP TABLE IF EXISTS `jv-it_static_pages`;
CREATE TABLE `jv-it_static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jv-it_static_pages
-- ----------------------------
INSERT INTO `jv-it_static_pages` VALUES ('1', 'Admin Help', 'admin-help', '<p><span style=\"font-size: 36pt;\">I. BUTTON USAGE</span></p>\r\n<p><img src=\"../../../assets/uploads/editor/button.png\" alt=\"\" border=\"0\" style=\"margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;\" /></p>\r\n<p><span style=\"font-size: 36pt;\">II. General page\'s explanation</span></p>\r\n<p>&nbsp;<img src=\"../../../assets/uploads/editor/Page.png\" alt=\"\" border=\"0\" style=\"margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;\" /></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: -webkit-xxx-large;\">III. Creat content page\'s explanation</span></p>\r\n<p><img src=\"../../../assets/uploads/editor/Content.png\" alt=\"\" border=\"0\" style=\"margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;\" /></p>\r\n<p><img src=\"../../../assets/uploads/editor/Hoi dap.png\" alt=\"\" border=\"0\" style=\"margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;\" /></p>', '0', '2015-06-09 10:07:40');
